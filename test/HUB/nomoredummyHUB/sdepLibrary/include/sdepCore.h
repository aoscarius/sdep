/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepCore.h
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#ifndef SDEP_CORE_H
#define SDEP_CORE_H

#include <stdint.h>
#include <stdlib.h>

#include "sdepConfig.h"
#include "sdepProtocol.h"
#include "sdepMessage.h"

// SDEP Internal versioning control
#if setMASTER == 0
/// Build a SDEP magic number from versioning numbers
#define MVER(mj, mn, sub, rel) ((uint32_t)(mj << 24) | (uint32_t)(mn << 16) | (uint32_t)(sub << 8) | (uint32_t)(rel << 0))
#else
/// Recovery SDEP version major number from a SDEP magic number
#define SDEPVER_mj(sdepmv)     ((sdepmv & 0xFF000000) >> 24)
/// Recovery SDEP version minor number from a SDEP magic number
#define SDEPVER_mn(sdepmv)     ((sdepmv & 0x00FF0000) >> 16)
/// Recovery SDEP subversion number from a SDEP magic number
#define SDEPVER_sub(sdepmv)    ((sdepmv & 0x0000FF00) >> 8)
/// Recovery SDEP release number from a SDEP magic number
#define SDEPVER_rel(sdepmv)    ((sdepmv & 0x000000FF) >> 0)
#endif

// SDEP Probe Protocol Definition
#define SDEPM    0x53444550 // "SDEP" String to identify the protocol
#define SDEPVER  (MVER(0, 9, 0, 0)) // SDEP version magic number

// SDEP returned function value
#define sdepOK           0x00
#define sdepERROR        0x01
#define sdepBADMESSAGE   0x02
#define sdepPAYTOOBIG    0x03
#define sdepNODEVADDR    0x04
#define sdepBADRESPONSE  0x05

// SDEP internal define
#define sdepBADDRESS     0xFF

#if setMASTER == 0
// -----------------------------
// ------ Slave Functions ------
// -----------------------------

typedef uint32_t pSerial_t;

typedef struct {
    int32_t (*writeBytes)(const uint8_t *buffer, uint16_t length);
    int32_t (*readBytes)(uint8_t *buffer, uint16_t length);
} phyDriver_t;

typedef struct {
    int8_t (*aliveCheck)();
    int8_t (*capabilityRequest)();
    int8_t (*configRequest)(sConfig_t sConfList[], pLength_t scNum);
    int8_t (*samplesRequest)(sId_t sensorId);
    int8_t (*samplesRequestInt)();
    int8_t (*customRequest)(mPayload_t *mPayload, pLength_t pLength);
    int8_t (*probeDisconnect)();
} sdepCallbacks_t;

typedef struct {
    uint32_t sMagic;
    uint32_t sMagicVer;
    mAddress_t mAddress;
    pSerial_t pSerial;
    struct {
        uint8_t Reserved:7;
        uint8_t pIntEnabled:1;
    } flags;
    sdepCallbacks_t sdepCallbacks;
    phyDriver_t phyDriver;
} sdepCore_t;

void   setInterrupt();
void   clearInterrupt();
int8_t isInterrupted();
int8_t sdepInitCore(pSerial_t pSerial, phyDriver_t phyDriver, sdepCallbacks_t sdepCallbacks);
int8_t sdepCoreMachineStep();
int8_t sdepMachineIsRunning();

#else
// -----------------------------
// ------ Master Functions -----
// -----------------------------

typedef struct {
    int32_t (*selectAddress)(uint8_t pAddress);
    int32_t (*writeBytes)(const uint8_t *buffer, uint16_t length);
    int32_t (*readBytes)(uint8_t *buffer, uint16_t length);
} phyDriver_t;

typedef struct {
    int8_t (*ackResponse)(mAddress_t pAddress);
    int8_t (*errorResponse)(mAddress_t pAddress, sError_t sErrorList[], uint16_t seNum);
    int8_t (*errorResponseInt)(mAddress_t pAddress);
    int8_t (*capabilityResponse)(mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum);
    int8_t (*dataArriveResponse)(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize);
    int8_t (*dataArriveResponseInt)(mAddress_t pAddress, sId_t sensorId, uint8_t intType, uint8_t samplesArray[], uint16_t arrSize);
    int8_t (*customResponse)(mAddress_t pAddress, mPayload_t *mPayload, pLength_t pLength);
} sdepCallbacks_t;

typedef struct {
    sdepCallbacks_t sdepCallbacks;
    phyDriver_t phyDriver;
} sdepCore_t;

int8_t sdepInitCore(phyDriver_t phyDriver, sdepCallbacks_t sdepCallbacks);
int8_t sdepCoreResponseDecoder(mAddress_t pAddress);

#endif //setMASTER

#endif //SDEP_CORE_H
