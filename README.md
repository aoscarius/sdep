# (SDEP) Simple Data Exchange Protocol

![Logo SDEP](docs/SDEP.png)

Born during the course of the Embedded System for the final year project, SDEP is not only a protocol but it is a framework (protocol + library + test suite) for data exchange between two heterogeneous systems with the Probe (Slave) and HUB (Master) roles respectively. Currently, only the USB driver is implemented.
All SDEP messages have a four byte header, and a variable size payload. The physical is not a constraint, any serial channel that can provide both read and write primitives is a valid physical.
More details are into offical [documentation](docs/DS0001-SDEP.pdf "SDEP official documentation") (Italian only) in the repository.

## 1 - Message Format

The standard format of a SDEP mesage is the current:

| mType | mAddress | pLength | mPayload |
|:-----:|:--------:|:-------:|:--------:|
|1 Byte |  1 Byte  |  2 Byte |  N Byte  |

The max size of a standard SDEP message is constrained from the max size of the pLength value ie 65 KB. To exceed the limit of physical used, the serializer divides large messages in chunks that are properly reassembled by deserializer.
The library support either the static and dynamic allocation of the memory for the serializer and for support the device with small memory limitations (e.g. Arduino), there is a revisited version of the protocol, called uSDEP, that provide to reduce the max size of a single message to 256 Byte with a zero padding of the MSB bytes of the *pLength*.

The uSDEP message have the curret format:

| mType | mAddress | pLength | pPadding | mPayload |
|:-----:|:--------:|:-------:|:--------:|:--------:|
|1 Byte |  1 Byte  |  1 Byte |  1 Byte  |  N Byte  |

## 2 - Message Type

The first byte of every message is an 1 byte identifier called **mType**. This value indicates the type of message being sent, and allows us to determine the format for the payload of the message.

|M/S| mType       | Id   | Description                         |
|:-:|:------------|:-----|:------------------------------------|
|M/S| NULL        | 0x00 | A null message descriptor           |
| M | WHO         | 0x01 | Request the plug-in data            |
| S | IAM         | 0x02 | Report the plug-in data             |
| M | YOUARE      | 0x03 | Assign an address to a Slave        |
| S | ACK         | 0x04 | Synchronous ACK response            |
| M | AREYOUALIVE | 0x05 | Request an heartbeat check          | 
| S | ALIVEACK    | 0x06 | Response at heartbeat check         |
| M | BYEBYE      | 0x07 | Request a disconnect of a Slave     |
| M | WHAT        | 0x08 | Request the capability of a Slave   |
| S | MYCAP       | 0x09 | Report the slave capability         |
| M | CONF        | 0x0A | Configure the slave sensors         |
| M | DREQ        | 0x0B | Request a specific sample's packet  |
| S | DEXG        | 0x0C | Send the request sample's packet    |
| S | ERROR       | 0x0D | Report an error in the request      |
| M | DREQIT      | 0x0E | Request a triggered sample's packet |
| S | DEXGIT      | 0x0F | Send the triggered sample's packet  |
| S | ERRORIT     | 0x10 | Report a bad triggered request      |
|M/S| CUSTOM      | 0x1F | Send a customized message           |
 
## 3 - Transaction

The protocol is divided in transactions. A SDEP transcatcion start with a Master write and terminate with a Master read (Master request + Slave Response). If one of these fails, the entire transaction fails.
For now there are only eigth valid transaction defined in the protocol. Any Master request allow only a specific range of response.

#### 3.1 - Handshake

![SDEP Transaction](docs/images/WHOIAM_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| WHO         | BADDR    | 0       | PADDR                             |
| S:| IAM         | PADDR    | PLEN    | SDEPM:SDEPV:SERIAL                |

![SDEP Transaction](docs/images/YOUAREACK_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| YOUARE      | BADDR    | PLEN    | PADDR                             |
| S:| ACK         | PADDR    | 0       | <p/>                              |

#### 3.2 - Heartbeat

![SDEP Transaction](docs/images/AYAACK_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| AREYOUALIVE | PADDR    | 0       | <p/>                              |
| S:| ALIVEACK    | PADDR    | 0       | <p/>                              |

#### 3.3 - Capability

![SDEP Transaction](docs/images/WHATMYCAP_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| WHAT        | PADDR    | 0       | <p/>                              |
| S:| MYCAP       | PADDR    | PLEN    | STYP:SQTY:SCAP-STYP:SQTY:SCAP-... |

#### 3.4 - Config

![SDEP Transaction](docs/images/CONFACK_trans.png)![SDEP Transaction](docs/images/CONFERROR_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| CONF        | PADDR    | PLEN    | SID:MODE:CONF-SID:MODE:CONF-...   |
| S:| ACK         | PADDR    | 0       | <p/>                              |
| S:| ERROR       | PADDR    | PLEN    | SID:ECOD-SID:ECOD-...             |

#### 3.5 - Data Exchange

![SDEP Transaction](docs/images/DREQDEXG_trans.png)![SDEP Transaction](docs/images/DREQERROR_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| DREQ        | PADDR    | PLEN    | SID                               |
| S:| DEXG        | PADDR    | PLEN    | SID:N0:N1:N2:N3:N4:N5:N6:N7:...   |
| S:| ERROR       | PADDR    | PLEN    | SID:ECOD                          |

#### 3.6 - Data Exchange with Interrupt

![SDEP Transaction](docs/images/DREQITDEXGIT_trans.png)![SDEP Transaction](docs/images/DREQITERRORIT_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| DREQIT      | PADDR    | 0       | <p/>                              |
| S:| DEXGIT      | PADDR    | PLEN    | SID:ITYP:N0:N1:N2:N3:N4:N5:N6:... |
| S:| ERRORIT     | PADDR    | 0       | <p/>                              |

#### 3.7 - Custom

![SDEP Transaction](docs/images/CUSTOM_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| CUSTOM      | PADDR    | PLEN    | C0:C1:C2:C3:C4:C5:C6:C7:C8:C9:... |
| S:| CUSTOM      | PADDR    | PLEN    | C0:C1:C2:C3:C4:C5:C6:C7:C8:C9:... |

#### 3.8 - Disconnect

![SDEP Transaction](docs/images/BYEBYEACK_trans.png)

|   | mType       | mAddress | pLength | mPayload                          |
|--:|:------------|:---------|:--------|:----------------------------------|
| M:| BYEBYE      | PADDR    | 0       | <p/>                              |
| S:| ACK         | PADDR    | 0       | <p/>                              |

## 4 - Evenent Triggering (soft interrupt)

The MSB bit of the *mType* field in the message, is used for the identification of the event triggering from the PROBE. The driver analyzes this bit in the heartbeat transaction response to trigger the interrupt event to the HUB.

## 5 - Endianness

All values larger than 8-bits must be encoded in little endian format. Actually there aren't any type of automatic control or conversion in the library so the protocol works only on the little-endian machine. 

## TODO: 
 
 * Implement the automatic conversion on endianess encoding
 * Implement the python porting of the library

## Contact

**Author**: [A[O]scar(ius)](https://gitlab.com/u/aoscarius)

**Co-Author**: Iorio Raffaele

**Git Repository**:  [https://gitlab.com/aoscarius/sdep](https://gitlab.com/aoscarius/sdep)
   
## License

SDEP by [A[O]scar(ius)](https://gitlab.com/u/aoscarius) & Iorio Raffaele is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)