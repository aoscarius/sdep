/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepMessage.c
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "sdepMessage.h"
#include "sdepCore.h"

#define TRANSFERQUANTUM 32
#define MIN(A, B) ((A < B)? A : B )

extern sdepCore_t sdepCore;

#if useMALLOC == 0
// --------------------------------------------------------------------
// ---------------- Version without use of the MALLLOC ----------------
// --------------------------------------------------------------------

/**
 * @brief   Create and initialize a new sdepMessage
 * @param   mType - type of message (see the define)
 * @param   mAddress - address of the PROBE
 * @param   pLength - length of the payload
 * @param   mPayload - initialized payload if any or null reference
 * @retval  A reference to a well formatted sdepMessage
 */
sdepMessage_t *newMessage(mType_t mType, mAddress_t mAddress, pLength_t pLength, mPayload_t *mPayload){
    static sdepMessage_t smessage;
    static mPayload_t smPayload[pSIZE_MAX];
        
    smessage.mType = mType;
    smessage.mAddress = mAddress;
    smessage.pLength = pLength;

// If is set uSDEP in Slave mode zero filled the padding
#if setMICROSDEP == 1 && setMASTER == 0
    smessage.pPadding = 0;
#endif
    
    smessage.mPayload = smPayload;

    if ((smessage.pLength>0) && (mPayload!=(void*)0)) {
        memcpy(smessage.mPayload, mPayload, pLength);
    }

    return &smessage;
}

/**
 * @brief   In the non malloc mode, this function is empty but is define for symmetry
 * @param   sdepMessage - a previous allocated sdepMessage
 * @retval  None
 */
void freeMessage(sdepMessage_t *sdepMessage){
}

/**
 * @brief   Send a previous allocated well formatted sdepMessage trough the PHY
 * @param   sdepMessage - a well formatted sdepMessage
 * @retval  Number of sending byte
 */
uint32_t sendMessage(sdepMessage_t *sdepMessage){
    int32_t sBytes;
    pLength_t actualSend;
    pLength_t numOfSends;
    pLength_t remainderBytes;
    
    if (sdepMessage != (void*)0) {

#if setMASTER == 1
        //Send a start transaction command to driver (NULL WRITE)
        sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage, 0);
#endif

        sBytes = sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage, headerSIZE);

        if (sdepMessage->pLength>0) {
            numOfSends = ((sdepMessage->pLength/TRANSFERQUANTUM) + 1);
            remainderBytes = sdepMessage->pLength;

            for (actualSend = 0; actualSend < numOfSends; actualSend++) {
                sBytes += sdepCore.phyDriver.writeBytes((uint8_t*)(sdepMessage->mPayload+(sdepMessage->pLength-remainderBytes)), MIN(remainderBytes, TRANSFERQUANTUM));
                remainderBytes -= TRANSFERQUANTUM;
            }
        }

        if (sBytes>=0) {
            return (uint32_t)sBytes;
        } else {
            return 0;
        }

    } else {
        
        return 0;
        
    }

}

/**
 * @brief   Receive a previous allocated well formatted sdepMessage trough the PHY
 * @param   sdepMessage - a well formatted sdepMessage
 * @retval  Number of received byte
 */
uint32_t recvMessage(sdepMessage_t *sdepMessage){
    int32_t rBytes;
    pLength_t actualRecv;
    pLength_t numOfRecvs;
    pLength_t remainderBytes;

    if (sdepMessage != (void*)0) {

        rBytes=sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage, headerSIZE);

        if (sdepMessage->pLength>0) {
            numOfRecvs = ((sdepMessage->pLength/TRANSFERQUANTUM) + 1);
            remainderBytes = sdepMessage->pLength;

            for (actualRecv = 0; actualRecv < numOfRecvs; actualRecv++) {
                rBytes+=sdepCore.phyDriver.readBytes((uint8_t*)(sdepMessage->mPayload+(sdepMessage->pLength-remainderBytes)), MIN(remainderBytes, TRANSFERQUANTUM));
                remainderBytes -= TRANSFERQUANTUM;
            }
        }

#if setMASTER == 1
        // Send end transaction command to driver (NULL READ)
        sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage, 0);
#endif

        if (rBytes>=0) {
            return (uint32_t)rBytes;
        } else {
            return 0;
        }

    } else {

        return 0;

    }
}

#else
// -----------------------------------------------------------------
// ---------------- Version with use of the MALLLOC ----------------
// -----------------------------------------------------------------

/**
 * @brief   Create and initialize a new sdepMessage
 * @param   mType - type of message (see the define)
 * @param   mAddress - address of the PROBE
 * @param   pLength - length of the payload
 * @param   mPayload - initialized payload if any or null reference
 * @retval  A reference to a well formatted sdepMessage
 */
sdepMessage_t *newMessage(mType_t mType, mAddress_t mAddress, pLength_t pLength, mPayload_t *mPayload){
    sdepMessage_t *dmessage = (sdepMessage_t*)malloc(sizeof(sdepMessage_t));
    mPayload_t *dmPayload = (void*)0;
    
    dmessage = (sdepMessage_t*)malloc(sizeof(sdepMessage_t));
    
    dmessage->mType = mType;
    dmessage->mAddress = mAddress;
    dmessage->pLength = pLength;

// If is set uSDEP in Slave mode zero filled the padding
#if setMICROSDEP == 1 && setMASTER == 0
    dmessage->pPadding = 0;
#endif

    dmessage->mPayload = dmPayload;

    if ((dmessage->pLength>0) && (mPayload!=(void*)0)) {
        dmessage->mPayload = (mPayload_t*)malloc(pLength*sizeof(mPayload_t));
        memcpy(dmessage->mPayload, mPayload, pLength);
    }

    return dmessage;
}

/**
 * @brief   In the malloc mode, this function free the heap memory
 *          from an allocated sdepMessage
 * @param   sdepMessage - a previous allocated sdepMessage
 * @retval  None
 */
void freeMessage(sdepMessage_t *sdepMessage){
    if (sdepMessage != (void*)0) {
        if (sdepMessage->mPayload != (void*)0) {
            free(sdepMessage->mPayload);
        }
        free(sdepMessage);
    }
}

/**
 * @brief   Send a previous allocated well formatted sdepMessage trough the PHY
 * @param   sdepMessage - a well formatted sdepMessage
 * @retval  Number of sending byte
 */
uint32_t sendMessage(sdepMessage_t *sdepMessage){
    int32_t sBytes;
    pLength_t actualSend;
    pLength_t numOfSends;
    pLength_t remainderBytes;
    
    if (sdepMessage != (void*)0) {

#if setMASTER == 1
        //Send a start transaction command to driver (NULL WRITE)
        sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage, 0);
#endif

        //Send a start transaction command to driver (NULL WRITE)
        sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage, headerSIZE);

        if (sdepMessage->pLength>0) {
            numOfSends = ((sdepMessage->pLength/TRANSFERQUANTUM) + 1);
            remainderBytes = sdepMessage->pLength;

            for (actualSend = 0; actualSend < numOfSends; actualSend++) {
                sBytes += sdepCore.phyDriver.writeBytes((uint8_t*)(sdepMessage->mPayload+(sdepMessage->pLength-remainderBytes)), MIN(remainderBytes, TRANSFERQUANTUM));
                remainderBytes -= TRANSFERQUANTUM;
            }
        }
    
        if (sBytes>=0) {
            return (uint32_t)sBytes;
        } else {
            return 0;
        }

    } else {
        
        return 0;
        
    }
}

/**
 * @brief   Receive a previous allocated well formatted sdepMessage trough the PHY
 * @param   sdepMessage - a well formatted sdepMessage
 * @retval  Number of received byte
 */
uint32_t recvMessage(sdepMessage_t *sdepMessage){
    int32_t rBytes;
    pLength_t actualRecv;
    pLength_t numOfRecvs;
    pLength_t remainderBytes;

    if (sdepMessage != (void*)0) {

        
        rBytes=sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage, headerSIZE);

        if (sdepMessage->pLength>0) {
            if (sdepMessage->mPayload == (void*)0) sdepMessage->mPayload=(mPayload_t*)malloc(sdepMessage->pLength);

            numOfRecvs = ((sdepMessage->pLength/TRANSFERQUANTUM) + 1);
            remainderBytes = sdepMessage->pLength;

            for (actualRecv = 0; actualRecv < numOfRecvs; actualRecv++) {
                rBytes+=sdepCore.phyDriver.readBytes((uint8_t*)(sdepMessage->mPayload+(sdepMessage->pLength-remainderBytes)), MIN(remainderBytes, TRANSFERQUANTUM));
                remainderBytes -= TRANSFERQUANTUM;
            }
        }
        
#if setMASTER == 1
        // Send end transaction command to driver (NULL READ)
        sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage, 0);
#endif

        if (rBytes>=0) {
            return (uint32_t)rBytes;
        } else {
            return 0;
        }

    } else {

        return 0;

    }
}

#endif //useMALLOC
