/**
 * @file:	main.c
 * @author: Castello Oscar, Iorio Raffaele
 * @date:	12 June 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rng.h"
#include "init_function.h"

#include "usbd_cdc_vcp.h"

#include "cmsis_os.h"

#include "sdepCore.h"

#define SERIAL 0x01234567

#define LED_GREEN	GPIO_PIN_12	// LED Verde (Left)
#define LED_ORANGE 	GPIO_PIN_13	// LED Arancio (Up)
#define LED_RED		GPIO_PIN_14	// LED Rosso (Right)
#define LED_BLUE 	GPIO_PIN_15	// LED Blu (Down)
#define LEDS 		(LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE)

#define RESET_TIMEOUT 10*1000 //ms
//uint32_t resetTick = 0;
//extern void NVIC_SystemReset(void);

RNG_HandleTypeDef RNGHandle;

void taskSDEPMachine(void const *args);
osThreadDef(sdepCORE, taskSDEPMachine, osPriorityNormal, 1, 512);
void taskSDEPIntSimul(void const *args);
osThreadDef(sdepINT, taskSDEPIntSimul, osPriorityNormal, 1, 128);

void sdepInit();
void ledsInit();

int8_t aliveReq();
int8_t capReq();
int8_t configReq(sConfig_t sConfList[], pLength_t scNum);
int8_t samplesReq(sId_t sensorId);
int8_t samplesReqInt();
int8_t customReq(mPayload_t *mPayload, pLength_t pLength);
int8_t probeDisc();

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
	/* STM32F4xx HAL library initialization:
	- Configure the Flash prefetch, instruction and Data caches
	- Configure the Systick to generate an interrupt each 1 msec
	- Set NVIC Group Priority to 4
	- Global MSP (MCU Support Package) initialization
	*/
	HAL_Init();
	// Setting of the correct clock configuration

	// Initialize leds
	ledsInit();
	// Initialize the sdepCore
	sdepInit();

    memset(&RNGHandle, 0, sizeof(RNG_HandleTypeDef));
	RNGHandle.Instance = RNG;
    __RNG_CLK_ENABLE();
    HAL_RNG_Init(&RNGHandle);

	// Setting correct clock for USB support
	SystemClock_Config();
	// Setting and enable USB CDC Interface
	USBConfig();

	osThreadCreate(osThread(sdepCORE), NULL);
	osThreadCreate(osThread(sdepINT), NULL);
	osKernelStart();

	/* Infinite loop */
	while (1){
	}
}

void taskSDEPMachine(void const *args){
	//resetTick = HAL_GetTick();

	while(1){
		sdepCoreMachineStep();
	}
}

void taskSDEPIntSimul(void const *args){
	while(1){
		// Generate an Interrupt every 2 sec
		setInterrupt();
		HAL_Delay(2000);
	}
}


void ledsInit(){
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Enable the GPIO_LED Clock */
	__GPIOD_CLK_ENABLE();

	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = LEDS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;

	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOD, LEDS, GPIO_PIN_RESET);
}


void sdepInit(){
	phyDriver_t phyDriver = {
		.readBytes=VCP_Read,
		.writeBytes=VCP_Write
	};

	sdepCallbacks_t sdepCallbacks = {
		.aliveCheck=aliveReq,
		.capabilityRequest=capReq,
		.configRequest=configReq,
		.samplesRequest=samplesReq,
		.samplesRequestInt=samplesReqInt,
		.customRequest=customReq,
		.probeDisconnect=probeDisc
	};

	sdepInitCore(SERIAL, phyDriver, sdepCallbacks);

}

int8_t aliveReq(){
	HAL_GPIO_WritePin(GPIOD, LED_GREEN, GPIO_PIN_SET);
	HAL_Delay(50);
	HAL_GPIO_WritePin(GPIOD, LED_GREEN, GPIO_PIN_RESET);
	HAL_Delay(50);

	//HAL_Delay(10000);

	// resetTick = HAL_GetTick();

	return sdepOK;
}

int8_t capReq(){
	sCapability_t sCapability;
	sCapability.sensorType=0x03;
	sCapability.sensorQuantity=0x02;
	sCapability.sensorCap.intSupport=1;
	sCapability.sensorCap.reqSupport=PERIODICMode;
	sCapability.sensorCap.sampleLen=1;
	uint16_t scNum=1;

	sendCapability(&sCapability,scNum);

	return sdepOK;
}

int8_t configReq(sConfig_t sConfList[], pLength_t scNum){
	int i, y = 0;
	if (scNum>0) {
		for (i=0; i<scNum; i++){
			if (sConfList[i].sensorId.sensorType==3) {
				if (sConfList[i].sensorId.sensorNum==0 || sConfList[i].sensorId.sensorNum==1){
					if (sConfList[i].sensorMode.reqSettings<PERIODICMode && sConfList[i].sensorMode.sActive==1 && sConfList[i].sensorConfig==0) y++;
				}
			}
		}
	}
	if (y == scNum) {
		sendAck();
	}
	return sdepOK;

}

int8_t samplesReq(sId_t sensorId){
	uint8_t samplesArray0 = 0xAA;
	uint8_t samplesArray1 = 0x4F;
	if (sensorId.sensorType==3 && sensorId.sensorNum==0) sendSamplesPacket(sensorId, &samplesArray0, sizeof(samplesArray0));
	if (sensorId.sensorType==3 && sensorId.sensorNum==1) sendSamplesPacket(sensorId, &samplesArray1, sizeof(samplesArray1));

	return sdepOK;
}

int8_t samplesReqInt(){
	sId_t sensorId;
	uint8_t i;
	uint32_t rndNumber;
	uint8_t samplesArray[100];

	for (i=0; i<100; i++){
		HAL_RNG_GenerateRandomNumber(&RNGHandle, &rndNumber);
		samplesArray[i] = rndNumber;
	}

	//uint8_t samplesArray[] = {0x4F, 0x00, 0x54, 0x45, 0x08, 0x54, 0x01, 0x53, 0x21, 0x06, 0x54, 0x06, 0x87, 0x30, 0x21, 0x32, 0x16, 0x05, 0x07, 0x68, 0x05, 0x43, 0x21, 0x36, 0x57, 0x06, 0x40, 0x13, 0x02, 0x10, 0x65 };

	if (isInterrupted()) {
		sensorId.sensorType = 3;
		sensorId.sensorNum = 0;
		sendSamplesPacketInt(sensorId, intTHRESHOLD, samplesArray, 100);
	} else {
		sendErrorInt();
	}

	clearInterrupt();

	return sdepOK;
}

int8_t customReq(mPayload_t *mPayload, pLength_t pLength){
	sendCustom(mPayload, pLength);
	return sdepOK;
}

int8_t probeDisc(){
	sendAck();
	USBDeplug();
	return sdepOK;
}

void HAL_SYSTICK_Callback(void){
/*
	if (sdepMachineIsRunning()){
		if (HAL_GetTick()-resetTick > RESET_TIMEOUT) NVIC_SystemReset();
	}
*/
}

