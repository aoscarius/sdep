/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepProtocol.c
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "sdepProtocol.h"
#include "sdepMessage.h"
#include "sdepCore.h"

extern sdepCore_t sdepCore;

#if setMASTER == 0
// -----------------------------
// ------ Slave Functions ------
// -----------------------------

/**
 * @brief   Send an ACK to the Master
 * @param   None
 * @retval  Zero on success or number of incomplete sending byte
 */
int32_t sendAck(){
    sdepMessage_t *message;
    pLength_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = ACK_mType;
    
    pLength = 0;
    
    message = newMessage(mType, sdepCore.mAddress, pLength, (void*)0);
    txByte=sendMessage(message);
    
    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

/**
 * @brief   Send an error list to the Master
 * @param   sErrorList[] - an array list of sError_t field
 * @param   seNum - number of sError_t field in mPayload
 * @retval  Zero on success or number of incomplete sending byte, else -sdepPAYTOBIG
 *          if the size of the payload is greather than its max size (in base of selected 
 *          protocol version)
 */
int32_t sendError(sError_t sErrorList[], uint16_t seNum){
    sdepMessage_t *message;
    pLength_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = ERROR_mType;

    pLength = (seNum * sizeof(sError_t));
    if (pLength > pSIZE_MAX)
        return -sdepPAYTOOBIG;

    message = newMessage(mType, sdepCore.mAddress, pLength, (mPayload_t*)sErrorList);
    txByte=sendMessage(message);

    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

/**
 * @brief   Send an error to the Master in interrupted mode
 * @param   None
 * @retval  Zero on success or number of incomplete sending byte
 */
int32_t sendErrorInt(){
    sdepMessage_t *message;
    pLength_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = ERRORIT_mType;
    
    pLength = 0;

    message = newMessage(mType, sdepCore.mAddress, pLength, (void*)0);
    txByte=sendMessage(message);

    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

/**
 * @brief   Send a capability list to the Master
 * @param   sCapsList[] - an array list of sCapability_t field
 * @param   scNum - number of sCapability_t field in mPayload
 * @retval  Zero on success or number of incomplete sending byte, else -sdepPAYTOBIG
 *          if the size of the payload is greather than its max size (in base of selected 
 *          protocol version)
 */
int32_t sendCapability(sCapability_t sCapsList[], uint16_t scNum){
    sdepMessage_t *message;
    pLength_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = MYCAP_mType;
    
    pLength = (scNum * sizeof(sCapability_t));
    if (pLength > pSIZE_MAX)
        return -sdepPAYTOOBIG;
    
    message = newMessage(mType, sdepCore.mAddress, pLength, (mPayload_t*)sCapsList);
    txByte=sendMessage(message);

    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

/**
 * @brief   Send a packet of samples to the Master
 * @param   sensorId - sId_t field to identify a precise sensor on the probe
 * @param   samplesArray[] - an array of samples to send
 * @param   arrSize - sizeof samplesArray
 * @retval  Zero on success or number of incomplete sending byte, else -sdepPAYTOBIG
 *          if the size of the payload is greather than its max size (in base of selected 
 *          protocol version)
 */
int32_t sendSamplesPacket(sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize){
    sdepMessage_t *message;
    pLength_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = DEXG_mType;
    
    pLength = (arrSize + sizeof(sId_t));
    if (pLength > pSIZE_MAX)
        return -sdepPAYTOOBIG;

#if useMALLOC == 0
    message = newMessage(mType, sdepCore.mAddress, 0, (void*)0);
	message->pLength = pLength;

    memcpy (&(message->mPayload[0]), &sensorId, sizeof(sId_t));
    memcpy (&(message->mPayload[sizeof(sId_t)]), samplesArray, arrSize);
#else
    mPayload_t *mPayload;
    mPayload=(mPayload_t*)malloc(sizeof(sId_t)+arrSize);

    memcpy (mPayload, &sensorId, sizeof(sId_t));
    memcpy (mPayload+sizeof(sId_t), samplesArray, arrSize);

    message = newMessage(mType, sdepCore.mAddress, pLength, (mPayload_t*)mPayload);

    free(mPayload);
#endif //useMALLOC
    
    txByte=sendMessage(message);
    
    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

/**
 * @brief   Send a packet of samples to the Master in response to interrupted request
 * @param   sensorId - sId_t field to identify a precise sensor on the probe
 * @param   intTypr - type of the generated interrupt
 * @param   samplesArray[] - an array of samples to send
 * @param   arrSize - sizeof samplesArray
 * @retval  Zero on success or number of incomplete sending byte, else -sdepPAYTOBIG
 *          if the size of the payload is greather than its max size (in base of selected 
 *          protocol version)
 */
int32_t sendSamplesPacketInt(sId_t sensorId, uint8_t intType, uint8_t samplesArray[], uint16_t arrSize){
    sdepMessage_t *message;
    pLength_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = DEXGIT_mType;

    pLength = (sizeof(sId_t) + sizeof(uint8_t) + arrSize);
    if (pLength > pSIZE_MAX)
        return -sdepPAYTOOBIG;

#if useMALLOC == 0
    message = newMessage(mType, sdepCore.mAddress, 0, (void*)0);
	message->pLength = pLength;

    memcpy (&(message->mPayload[0]), &sensorId, sizeof(sId_t));
    memcpy (&(message->mPayload[sizeof(sId_t)]), &intType, sizeof(uint8_t));
    memcpy (&(message->mPayload[sizeof(sId_t)+sizeof(uint8_t)]), samplesArray, arrSize);
#else
    mPayload_t *mPayload;
    mPayload=(mPayload_t*)malloc(sizeof(sId_t) + sizeof(uint8_t) + arrSize);

    memcpy (mPayload, &sensorId, sizeof(sId_t));
    memcpy (mPayload+sizeof(sId_t), &intType, sizeof(uint8_t));
    memcpy (mPayload+sizeof(sId_t)+sizeof(uint8_t), samplesArray, arrSize);

    message = newMessage(mType, sdepCore.mAddress, pLength, (mPayload_t*)mPayload);

    free(mPayload);
#endif //useMALLOC

    txByte=sendMessage(message);

    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}


/**
 * @brief   Send a custom packet with custom payload to the Master
 * @param   mPayload - a pointer to a raw payload
 * @param   pLength - the size of payload in byte
 * @retval  Zero on success or number of incomplete sending byte, else -sdepPAYTOBIG
 *          if the size of the payload is greather than its max size (in base of selected 
 *          protocol version)
 */
int32_t sendCustom(uint8_t *mPayload, uint16_t pLength){
    sdepMessage_t *message;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = CUSTOM_mType;

    if (pLength > pSIZE_MAX)
        return -sdepPAYTOOBIG;

    message = newMessage(mType, sdepCore.mAddress, pLength, (mPayload_t*)mPayload);
    txByte=sendMessage(message);

    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

#else
// -----------------------------
// ------ Master Functions -----
// -----------------------------

/**
 * @brief   Send a BYEBYE message to the Slave
 * @param   pAddress - address of selected probe where to send and receive
 * @retval  sdepOK if success, else -sdepNODEVADDRESS if is a bad address, else 
 *          -sdepBADMESSAGE if received a malformed message (i.e. for timeout), 
 *          -sdepBADRESPONSE if no receive an ACK
 */
int8_t sendDisconnect(uint8_t pAddress){
    sdepMessage_t *message;
    uint16_t pLength;
    uint32_t txByte;
    
    int8_t retVal = -sdepBADRESPONSE;

    mType_t mType;

    mType.intEnabled = 0;
    mType.msgType = BYEBYE_mType;
    
    if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
        return -sdepNODEVADDR;

    pLength = 0;
        
    message = newMessage(mType, pAddress, pLength, (void*)0);
    txByte=sendMessage(message);

    freeMessage(message);
    
    mType.intEnabled = 0;
    mType.msgType = NULL_mType;

    message = newMessage(mType, 0, 0, (void*)0);
    txByte=recvMessage(message);
    
    if ((txByte >= headerSIZE) && (txByte == (headerSIZE+message->pLength))){
        // Check if this message arrive from pAddress
        if (message->mAddress == pAddress) {
            if (message->mType.msgType == ACK_mType)
                retVal = sdepOK;

            if ((message->mType.msgType == ERROR_mType) && (message->pLength == 0)) 
                retVal = -sdepBADMESSAGE;
        }
    }

    freeMessage(message);

    return retVal;
}


/**
 * @brief   Send the request of capability to a specified slave PROBE through the its address
 * @param   pAddress - address of selected probe where to send and receive
 * @retval  sdepOK if success, else -sdepNODEVADDRESS if is a bad address,
 *          else -sdepBADMESSAGE if a malformed message is received from decoder 
 *          (i.e. for timeout), else the return value of the corresponding 
 *          activated callback
 */
int8_t sendCapabilityRequest(uint8_t pAddress){
    sdepMessage_t *message;
    uint16_t pLength;
    uint32_t txByte;
    
    int8_t retVal;
    
    mType_t mType;

    mType.intEnabled = 0;
    mType.msgType = WHAT_mType;
    
    if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
        return -sdepNODEVADDR;

    pLength = 0;
        
    message = newMessage(mType, pAddress, pLength, (void*)0);
    txByte=sendMessage(message);

    freeMessage(message);
    
    retVal = sdepCoreResponseDecoder(pAddress);
    
    switch(retVal) {
        case MYCAP_mType:
            return sdepOK;
            break;
        case -sdepNODEVADDR:
            return -sdepNODEVADDR;
            break;
        case -sdepBADMESSAGE:
            return -sdepBADMESSAGE;
            break;
        default:
            return retVal;
            break;
    }
}

/**
 * @brief   Send the config list of a specified PROBE through the its address
 * @param   pAddress - address of selected probe where to send and receive
 * @param   sConfList[] - an array list of sConfig_t field
 * @param   scNum - number of sConfig_t field
 * @retval  sdepOK if success, else -sdepPAYTOOBIG if the size of the payload 
 *          is greather than its max size, else -sdepNODEVADDRESS if is a bad address,
 *          else -sdepBADMESSAGE if a malformed message is received from decoder 
 *          (i.e. for timeout), else the return value of the corresponding 
 *          activated callback
 */
int8_t sendConfigTo(uint8_t pAddress, sConfig_t sConfList[], uint16_t scNum){
    sdepMessage_t *message;
    uint16_t pLength;
    uint32_t txByte;
    
    int8_t retVal;
    
    mType_t mType;

    mType.intEnabled = 0;
    mType.msgType = CONF_mType;

    if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
        return -sdepNODEVADDR;

    pLength = (scNum * sizeof(sConfig_t));
    if (pLength > pSIZE_MAX)
        return -sdepPAYTOOBIG;
            
    message = newMessage(mType, pAddress, pLength, (mPayload_t*)sConfList);
    txByte=sendMessage(message);

    freeMessage(message);
    
    retVal = sdepCoreResponseDecoder(pAddress);
    
    switch(retVal) {
        case ACK_mType:
        case ERROR_mType:
            return sdepOK;
            break;
        case -sdepNODEVADDR:
            return -sdepNODEVADDR;
            break;
        case -sdepBADMESSAGE:
            return -sdepBADMESSAGE;
            break;
        default:
            return retVal;
            break;
    }
}

/**
 * @brief   Send the request of samples packet to a specified slave PROBE:SID 
 *          through the its address
 * @param   pAddress - address of selected probe where to send and receive
 * @param   sensorId - sId_t field to identify a precise sensor on the probe
 * @retval  sdepOK if success, else -sdepNODEVADDRESS if is a bad address,
 *          else -sdepBADMESSAGE if a malformed message is received from decoder 
 *          (i.e. for timeout), else the return value of the corresponding 
 *          activated callback
 */
int8_t sendSamplesRequest(uint8_t pAddress, sId_t sensorId){
    sdepMessage_t *message;
    sId_t recvSID;
    uint16_t pLength;
    uint32_t txByte;
    
    int8_t retVal;
    
    mType_t mType;

    mType.intEnabled = 0;
    mType.msgType = DREQ_mType;
    
    if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
        return -sdepNODEVADDR;

    pLength = sizeof(sId_t);
        
    message = newMessage(mType, pAddress, pLength, (mPayload_t*)&sensorId);
    txByte=sendMessage(message);

    freeMessage(message);
    
    retVal = sdepCoreResponseDecoder(pAddress);
    
    switch(retVal) {
        case DEXG_mType:
        case ERROR_mType:
            return sdepOK;
            break;
        case -sdepNODEVADDR:
            return -sdepNODEVADDR;
            break;
        case -sdepBADMESSAGE:
            return -sdepBADMESSAGE;
            break;
        default:
            return retVal;
            break;
    }
}

/**
 * @brief   Send the request of samples packet to a specified slave PROBE through 
 *          the its address in interrupted mode
 * @param   pAddress - address of selected probe where to send and receive
 * @retval  sdepOK if success, else -sdepNODEVADDRESS if is a bad address,
 *          else -sdepBADMESSAGE if a malformed message is received from decoder 
 *          (i.e. for timeout), else the return value of the corresponding 
 *          activated callback
 */
int8_t sendSamplesRequestInt(uint8_t pAddress){
    sdepMessage_t *message;
    sId_t recvSID;
    uint16_t pLength;
    uint32_t txByte;

    int8_t retVal;
    
    mType_t mType;

    mType.intEnabled = 0;
    mType.msgType = DREQIT_mType;
    
    if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
        return -sdepNODEVADDR;

    pLength = 0;
        
    message = newMessage(mType, pAddress, pLength, (void*)0);
    txByte=sendMessage(message);

    freeMessage(message);
    
    retVal = sdepCoreResponseDecoder(pAddress);
    
    switch(retVal) {
        case DEXGIT_mType:
        case ERRORIT_mType:
            return sdepOK;
            break;
        case -sdepNODEVADDR:
            return -sdepNODEVADDR;
            break;
        case -sdepBADMESSAGE:
            return -sdepBADMESSAGE;
            break;
        default:
            return retVal;
            break;
    }
}

/**
 * @brief   Send the config list of a specified PROBE through the its address
 * @param   pAddress - address of selected probe where to send and receive
 * @param   mPayload - custom payload to send
 * @param   pLength - size in byte of custom payload to send
 * @retval  sdepOK if success, else -sdepPAYTOOBIG if the size of the payload 
 *          is greather than its max size, else -sdepNODEVADDRESS if is a bad address,
 *          else -sdepBADMESSAGE if a malformed message is received from decoder 
 *          (i.e. for timeout), else the return value of the corresponding 
 *          activated callback
 */
int8_t sendCustom(uint8_t pAddress, mPayload_t *mPayload, uint16_t pLength){
    sdepMessage_t *message;
    uint32_t txByte;
    
    int8_t retVal;

    mType_t mType;

    mType.intEnabled = 0;
    mType.msgType = CUSTOM_mType;
    
    if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
        return -sdepNODEVADDR;

    if (pLength > pSIZE_MAX)
        return -sdepPAYTOOBIG;
            
    message = newMessage(mType, pAddress, pLength, mPayload);
    txByte=sendMessage(message);

    freeMessage(message);
    
    retVal = sdepCoreResponseDecoder(pAddress);

    switch(retVal) {
        case CUSTOM_mType:
            return sdepOK;
            break;
        case -sdepNODEVADDR:
            return -sdepNODEVADDR;
            break;
        case -sdepBADMESSAGE:
            return -sdepBADMESSAGE;
            break;
        default:
            return retVal;
            break;
    }
}

#endif //setMASTER
