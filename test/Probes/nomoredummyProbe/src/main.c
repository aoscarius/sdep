/**
 * @file:	main.c
 * @author: Castello Oscar, Iorio Raffaele
 * @date:	12 June 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_iwdg.h"
#include "init_function.h"

#include "usbd_cdc_vcp.h"

#include "cmsis_os.h"

#include "sdepCore.h"

#include "lis3dsh.h"

#define SERIAL 0x01234567

#define LED_GREEN	GPIO_PIN_12	// LED Verde (Left)
#define LED_ORANGE 	GPIO_PIN_13	// LED Arancio (Up)
#define LED_RED		GPIO_PIN_14	// LED Rosso (Right)
#define LED_BLUE 	GPIO_PIN_15	// LED Blu (Down)
#define LEDS 		(LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE)


#define RESET_TIMEOUT 10*1000 //ms
//uint32_t resetTick = 0;
//extern void NVIC_SystemReset(void);

void taskSDEPMachine(void const *args);
osThreadDef(sdepCORE, taskSDEPMachine, osPriorityNormal, 1, 512);

void sdepInit();
void ledsInit();
void LIS3DSHInit();
void initMyCap();



int8_t aliveReq();
int8_t capReq();
int8_t configReq(sConfig_t sConfList[], pLength_t scNum);
int8_t samplesReq(sId_t sensorId);
int8_t samplesReqInt();
int8_t customReq(mPayload_t *mPayload, pLength_t pLength);
int8_t probeDisc();

//buffer read mems
#define MAX_MEMS_BUF_LENGTH	750
int16_t buffer0_MEMS[MAX_MEMS_BUF_LENGTH];
int16_t buffer1_MEMS[MAX_MEMS_BUF_LENGTH];
int16_t* pToSendMems=buffer0_MEMS;
int16_t* pToSampleMems=buffer0_MEMS;
int16_t* pTempMems=buffer1_MEMS;

//buffer read temp
#define MAX_TEMP_BUF_LENGTH 100
uint8_t buffer0_TEMP [MAX_TEMP_BUF_LENGTH];
uint8_t buffer1_TEMP [MAX_TEMP_BUF_LENGTH];
uint8_t* pToSendTemp=buffer0_TEMP;
uint8_t* pToSampleTemp=buffer0_TEMP;
uint8_t* pTempTemp=buffer1_TEMP;



//MyCap
#define NumberSensor 2
sCapability_t sCapability[NumberSensor];
uint16_t scNum = NumberSensor;


//Error Conf
sError_t MyError[10];
#define ErrorWrongSId 1
#define ErrorWrongConf 2
#define ErrorNoActive 3


typedef struct{
	sConfig_t sConfig;
	uint8_t myConfig; //period or threshold
	uint16_t index; //index for buffer
	void (*getSamples)(); //callback function to get a sample
}MyConfigure_t;

MyConfigure_t mConf[NumberSensor];


// Int vector
#define MAX_NUM_INT 10
typedef struct{
	sId_t sensorId;
	uint8_t interruptType;
}INTBuffer_t;

INTBuffer_t INTBuffer[MAX_NUM_INT];

void pushINT(INTBuffer_t*);
INTBuffer_t* popINT();
uint8_t head=0;
uint8_t tail=0;

void getSamples_TEMP();
void getSamples_MEMS();




/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
	/* STM32F4xx HAL library initialization:
	- Configure the Flash prefetch, instruction and Data caches
	- Configure the Systick to generate an interrupt each 1 msec
	- Set NVIC Group Priority to 4
	- Global MSP (MCU Support Package) initialization
	*/
	HAL_Init();
	// Setting of the correct clock configuration

	// Initialize leds
	ledsInit();
	// Initialize the sdepCore
	sdepInit();

	//initialize MEMS
	LIS3DSHInit();

	//initialize MY CAP
	initMyCap();

	// Setting correct clock for USB support
	SystemClock_Config();
	// Setting and enable USB CDC Interface
	USBConfig();


	osThreadCreate(osThread(sdepCORE), NULL);
	osKernelStart();

	/* Infinite loop */
	while (1){
	}
}

void taskSDEPMachine(void const *args){
	//resetTick = HAL_GetTick();

	while(1){
		sdepCoreMachineStep();
	}
}

void ledsInit(){
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Enable the GPIO_LED Clock */
	__GPIOD_CLK_ENABLE();

	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = LEDS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;

	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOD, LEDS, GPIO_PIN_RESET);
}


void sdepInit(){
	phyDriver_t phyDriver = {
		.readBytes=VCP_Read,
		.writeBytes=VCP_Write
	};

	sdepCallbacks_t sdepCallbacks = {
		.aliveCheck=aliveReq,
		.capabilityRequest=capReq,
		.configRequest=configReq,
		.samplesRequest=samplesReq,
		.samplesRequestInt=samplesReqInt,
		.customRequest=customReq,
		.probeDisconnect=probeDisc
	};

	sdepInitCore(SERIAL, phyDriver, sdepCallbacks);

}

int8_t aliveReq(){
	HAL_GPIO_WritePin(GPIOD, LED_GREEN, GPIO_PIN_SET);
	HAL_Delay(50);
	HAL_GPIO_WritePin(GPIOD, LED_GREEN, GPIO_PIN_RESET);
	HAL_Delay(50);
	//HAL_Delay(5000);

	// resetTick = HAL_GetTick();

	return sdepOK;
}

int8_t capReq(){
	sendCapability(sCapability,scNum);
	return sdepOK;
}

int8_t configReq(sConfig_t sConfList[], pLength_t scNum){
	int i;
	int errNum=0;
	if (scNum>0) {
		for (i=0; i<scNum; i++){
			switch(sConfList[i].sensorId.sensorType){
			case 1:
				if( (sConfList[i].sensorMode.reqSettings <= sCapability[0].sensorCap.reqSupport) && (sConfList[i].sensorMode.intSetting <= sCapability[0].sensorCap.intSupport) ){
					mConf[0].sConfig.sensorMode.sActive = sConfList[i].sensorMode.sActive;
					mConf[0].sConfig.sensorMode.intSetting = sConfList[i].sensorMode.intSetting;
					mConf[0].sConfig.sensorMode.reqSettings = sConfList[i].sensorMode.reqSettings;
					mConf[0].sConfig.sensorConfig = sConfList[i].sensorConfig;
					mConf[0].myConfig=sConfList[i].sensorConfig;
				}else{
					errNum++;
					MyError[errNum-1].sensorId.sensorType = sConfList[i].sensorId.sensorType;
					MyError[errNum-1].errorCode=ErrorWrongConf;
				}
				break;

			case 2:
				if( (sConfList[i].sensorMode.reqSettings <= sCapability[1].sensorCap.reqSupport) && (sConfList[i].sensorMode.intSetting <= sCapability[1].sensorCap.intSupport) ){
					mConf[1].sConfig.sensorMode.sActive = sConfList[i].sensorMode.sActive;
					mConf[1].sConfig.sensorMode.intSetting = sConfList[i].sensorMode.intSetting;
					mConf[1].sConfig.sensorMode.reqSettings = sConfList[i].sensorMode.reqSettings;
					mConf[1].sConfig.sensorConfig = sConfList[i].sensorConfig;
					mConf[1].myConfig=sConfList[i].sensorConfig;
				}else{
					errNum++;
					MyError[errNum-1].sensorId.sensorType=sConfList[i].sensorId.sensorType;
					MyError[errNum-1].errorCode=ErrorWrongConf;
				}
				break;

			default:
				errNum++;
				MyError[errNum-1].sensorId.sensorType=sConfList[i].sensorId.sensorType;
				MyError[errNum-1].errorCode=ErrorWrongSId;
				break;


			}
		}

	if (errNum==0) sendAck();
	else	sendError(MyError,errNum);
	}
	return sdepOK;

}

int8_t samplesReq(sId_t sensorId){
	switch(sensorId.sensorType){
	case 1:
		if (mConf[0].sConfig.sensorMode.sActive == 1){
			if (mConf[0].sConfig.sensorMode.reqSettings == ONESHOTMode){
				mConf[0].getSamples();
				sendSamplesPacket(sensorId, (uint8_t*)pToSampleMems,6);
			}else{
				sendSamplesPacket(sensorId, (uint8_t*)pToSendMems,mConf[0].index*2 );
				mConf[0].index=0;
				}
		}else{
			MyError[0].sensorId = sensorId;
			MyError[0].errorCode = ErrorNoActive;
			sendError(MyError,1);
		}
		break;
	case 2:
		if (mConf[1].sConfig.sensorMode.sActive == 1){
			if (mConf[1].sConfig.sensorMode.reqSettings == ONESHOTMode){
				mConf[1].getSamples();
				sendSamplesPacket(sensorId, (uint8_t*)pToSampleTemp,1);
			}else{
				sendSamplesPacket(sensorId, (uint8_t*)pToSendTemp,mConf[1].index);
				mConf[1].index = 0;
				}
		}else{
			MyError[0].sensorId = sensorId;
			MyError[0].errorCode = ErrorNoActive;
			sendError(MyError,1);
		}
		break;
	default:
		MyError[0].sensorId = sensorId;
		MyError[0].errorCode = ErrorWrongSId;
		sendError(MyError,1);
		break;
	}
	return sdepOK;
}

int8_t samplesReqInt(){
	INTBuffer_t* temp;
	if (isInterrupted()) {
		temp=popINT();
		if(temp->interruptType==intOVERFLOW)
			sendSamplesPacketInt(temp->sensorId,temp->interruptType, 0,0);
		else{
			switch(temp->sensorId.sensorType){
			case 1:
				sendSamplesPacketInt(temp->sensorId, temp->interruptType, (uint8_t*)pToSendMems, MAX_MEMS_BUF_LENGTH*2);
				pToSendMems=pToSampleMems;
				break;
			case 2:
				sendSamplesPacketInt(temp->sensorId, temp->interruptType, (uint8_t*)pToSendTemp, MAX_TEMP_BUF_LENGTH);
				pToSendTemp=pToSampleTemp;
				break;
			default:
				break;
			}
		}

	//	sendSamplesPacketInt(sensorId, intOVERFLOW, (uint8_t*)pToSend, MAX_MEMS_BUF_LENGTH*2);
	//	pToSend=pToSample;
		//start=0;
		//finish=0;
	} else {
		sendErrorInt();
	}

	clearInterrupt();

	return sdepOK;
}

int8_t customReq(mPayload_t *mPayload, pLength_t pLength){
	sendCustom(mPayload, pLength);
	return sdepOK;
}

int8_t probeDisc(){
	sendAck();
	USBDeplug();
	return sdepOK;
}

void HAL_SYSTICK_Callback(void){
	int i=0;
	for(i=0;i<NumberSensor;i++){
		if ( (mConf[i].sConfig.sensorMode.sActive == 1) && (mConf[i].sConfig.sensorMode.reqSettings != ONESHOTMode) ){
			if (mConf[i].sConfig.sensorMode.reqSettings == PERIODICMode){
				mConf[i].myConfig--;
				if (mConf[i].myConfig==0)
					mConf[i].getSamples();
			}
		}
	}
/*
	if (sdepMachineIsRunning()){
		if (HAL_GetTick()-resetTick > RESET_TIMEOUT) NVIC_SystemReset();
	}
*/
}


void LIS3DSHInit(){
	uint16_t ctrl = 0x0000;
	LIS3DSH_InitTypeDef l1s3dsh_InitStruct;
	int i;

	for (i=0;i<MAX_MEMS_BUF_LENGTH;i++){
		buffer0_MEMS[i]=0;
		buffer1_MEMS[i]=0;
	}

	/* Set configuration of LIS3DSH MEMS Accelerometer **********************/
	l1s3dsh_InitStruct.Output_DataRate = LIS3DSH_DATARATE_800;
	l1s3dsh_InitStruct.Axes_Enable = LIS3DSH_XYZ_ENABLE;
	l1s3dsh_InitStruct.SPI_Wire = LIS3DSH_SERIALINTERFACE_4WIRE;
	l1s3dsh_InitStruct.Self_Test = LIS3DSH_SELFTEST_NORMAL;
	l1s3dsh_InitStruct.Full_Scale = LIS3DSH_FULLSCALE_6;
	l1s3dsh_InitStruct.Filter_BW = LIS3DSH_FILTER_BW_800;

	/* Configure MEMS: power mode(ODR) and axes enable */
	ctrl = (uint16_t) (l1s3dsh_InitStruct.Output_DataRate | \
				   l1s3dsh_InitStruct.Axes_Enable);

	/* Configure MEMS: full scale and self test */
	ctrl |= (uint16_t) ((l1s3dsh_InitStruct.SPI_Wire    | \
					 l1s3dsh_InitStruct.Self_Test   | \
					 l1s3dsh_InitStruct.Full_Scale  | \
					 l1s3dsh_InitStruct.Filter_BW) << 8);

	/* Configure the accelerometer main parameters */
	LIS3DSH_Init(ctrl);
}

void initMyCap(){
	sCapability[0].sensorType=0x01;
	sCapability[0].sensorQuantity=0x01;
	sCapability[0].sensorCap.intSupport=1;
	sCapability[0].sensorCap.reqSupport=PERIODICMode;
	sCapability[0].sensorCap.sampleLen=6;
	mConf[0].getSamples=getSamples_MEMS;
	mConf[0].index=0;


	sCapability[1].sensorType=0x02;
	sCapability[1].sensorQuantity=0x01;
	sCapability[1].sensorCap.intSupport=1;
	sCapability[1].sensorCap.reqSupport=PERIODICMode;
	sCapability[1].sensorCap.sampleLen=1;
	mConf[1].getSamples=getSamples_TEMP;
	mConf[1].index=0;

}


void getSamples_MEMS(){
	INTBuffer_t temp;
	LIS3DSH_ReadACC(&pToSampleMems[mConf[0].index]);
	if (mConf[0].sConfig.sensorMode.reqSettings != ONESHOTMode) {
			mConf[0].myConfig=mConf[0].sConfig.sensorConfig; //reset timeout
			mConf[0].index = mConf[0].index+3;
			if (mConf[0].index == MAX_MEMS_BUF_LENGTH)	{
				mConf[0].index=0;
				if (mConf[0].sConfig.sensorMode.intSetting == 1){
					if (pTempMems==pToSendMems){ //caso in cui al precedente riempimento di buffer non è stato svuotato =>overflow
						pTempMems=pToSampleMems;
						pToSampleMems=pToSendMems;
						temp.sensorId.sensorType=1;
						temp.sensorId.sensorNum=0;
						temp.interruptType = intOVERFLOW;
						pushINT(&temp);
						setInterrupt();
					}else{ //caso data ready
					pToSampleMems=pTempMems;
					pTempMems=pToSendMems;
					temp.sensorId.sensorType=1;
					temp.sensorId.sensorNum=0;
					temp.interruptType = intDATAPACK;
					pushINT(&temp);
					setInterrupt();//for data ready
					}
				}else{ //overflow con periodo senza int
					pTempMems=pToSampleMems;
					pToSampleMems=pToSendMems;
					temp.sensorId.sensorType=1;
					temp.sensorId.sensorNum=0;
					temp.interruptType = intOVERFLOW;
					pushINT(&temp);
					setInterrupt();
				}
			}
	}

}

void getSamples_TEMP(){
	INTBuffer_t temp;
	ACCELERO_IO_Read(&pToSampleTemp[mConf[1].index],LIS3DSH_OUT_T_ADDR,1);
	if (mConf[1].sConfig.sensorMode.reqSettings != ONESHOTMode) {
			mConf[1].myConfig=mConf[1].sConfig.sensorConfig; //reset timeout
			mConf[1].index++;
			if (mConf[1].index == MAX_TEMP_BUF_LENGTH)	{
				mConf[1].index = 0;
				if (mConf[1].sConfig.sensorMode.intSetting == 1){
					if (pTempTemp==pToSendTemp){ //caso in cui al precedente riempimento di buffer non è stato svuotato =>overflow
						pTempTemp=pToSampleTemp;
						pToSampleTemp=pToSendTemp;
						temp.sensorId.sensorType=2;
						temp.sensorId.sensorNum=0;
						temp.interruptType = intOVERFLOW;
						pushINT(&temp);
						setInterrupt();
					}else{ //caso data ready
					pToSampleTemp=pTempTemp;
					pTempTemp=pToSendTemp;
					temp.sensorId.sensorType=2;
					temp.sensorId.sensorNum=0;
					temp.interruptType = intDATAPACK;
					pushINT(&temp);
					setInterrupt();//for data ready
					}
				}else{ //overflow con periodo senza int
					pTempTemp=pToSampleTemp;
					pToSampleTemp=pToSendTemp;
					temp.sensorId.sensorType=2;
					temp.sensorId.sensorNum=0;
					temp.interruptType = intOVERFLOW;
					pushINT(&temp);
					setInterrupt();
				}

			}
	}
}

void pushINT(INTBuffer_t* temp){
	INTBuffer[tail].sensorId.sensorType = temp->sensorId.sensorType;
	INTBuffer[tail].sensorId.sensorNum = temp->sensorId.sensorNum;
	INTBuffer[tail].interruptType = temp->interruptType;
	tail=(tail+1)%MAX_NUM_INT;
}

INTBuffer_t* popINT(){
	INTBuffer_t* temp=&INTBuffer[head];
	head=(head+1)%MAX_NUM_INT;
	return temp;
}
