/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @subproject: SDEP USB Physical Driver v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepUSBPHY_main.c
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include <linux/init.h>           // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>         // Core header for loading LKMs into the kernel
#include <linux/device.h>         // Header to support the kernel Driver Model
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
#include <linux/fs.h>             // Header for the Linux file system support
#include <linux/types.h>          // Typedef for uintX_t
#include <asm/uaccess.h>          // Required for the copy to user function
#include <linux/slab.h>           // Required for the kmalloc

#include <linux/sched.h>          // Required for recovery task from pid
#include <linux/signal.h>         // Required for signal functionality
#include <asm/siginfo.h>          // Required for signal struct info

#include <linux/timer.h>          // Support for timer functionality

#include "sdepPHYUSB.h"           // Support utility

#define SDEPUSBVERSION "0.9.0-rc"

/* Define these values to match your devices */
#define USB_SDEP_VENDOR_ID    0x0483
#define USB_SDEP_PRODUCT_ID   0x5740
#define USB_SDEP_IF_CLASS     0x0A  //CDC Data Class
#define USB_SDEP_IF_SCLASS    0x00
#define USB_SDEP_IF_PROTO     0x00

MODULE_LICENSE("GPL");
MODULE_AUTHOR("SE2Group driver for USB SDEP physical probe");

#define INC_USECOUNT();  try_module_get(THIS_MODULE);
#define DEC_USECOUNT();  module_put(THIS_MODULE);

/* External definable parameters */
// AreYouAlive repetition time parameter
int ayaInterval = 1; //s
module_param(ayaInterval, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP); //s
MODULE_PARM_DESC(ayaInterval, "[s] aliveCheck repetition time in seconds (default 1 seconds - 0 to disable)");
// Number of consecutive timeout for disconnect
__u8 numAyaTimedouts = 10; //N
module_param(numAyaTimedouts, byte, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP); //N
MODULE_PARM_DESC(numAyaTimedouts, "[N] number of consecutive timeout for disconnect (default 10 - range[1..255])");
// Sync Read-Write timeout param
int usbTimeout = 1000; //ms
module_param(usbTimeout, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP); //ms
MODULE_PARM_DESC(usbTimeout, "[ms] usb synchronous RW timeout in milliseconds (default 1000 ms - 0 to infinite)");
// A max timeout for a SDEP transaction
int transTimedout = 2000; //ms
module_param(transTimedout, uint, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP); //ms
MODULE_PARM_DESC(transTimedout, "[ms] sdep transaction timeout in milliseconds (default 2000 ms)");

// Timer definition for checkAlive sub-process
static struct timer_list ayaTimer;
void ayaTimerHandler(unsigned long data);
void usbphy_ayatxcomplete(struct urb *urb);
void usbphy_ayarxcomplete(struct urb *urb);

// Device driver basic structures
static int            majorNumber;   
static struct class*  sdepUSBClass  = NULL;
static struct device* sdepUSBDevice = NULL;

// SDEP driver internal informations
static uint8_t openedDEV = 0;
static uint8_t connectedDEV = 0;
static uint8_t currentSelected = 0;

static int currentPPID = 0;
static uint8_t intQueue[MAX_ENTRIES];

static int usbphy_probe(struct usb_interface *interface, const struct usb_device_id *id);
static void usbphy_disconnect(struct usb_interface *interface);
static void usbphy_softdisconnect(sdepUSBPHY_t *sdepUSBPHY);

static struct usb_device_id usbphy_table[] =
{
    { USB_DEVICE_AND_INTERFACE_INFO(USB_SDEP_VENDOR_ID, USB_SDEP_PRODUCT_ID, USB_SDEP_IF_CLASS, USB_SDEP_IF_SCLASS, USB_SDEP_IF_PROTO) },
    {} /* Terminating entry */
};
MODULE_DEVICE_TABLE (usb, usbphy_table);
 
static struct usb_driver usbphy_driver =
{
    .name =       USB_SDEP_UNAME,
    .id_table =   usbphy_table,
    .probe =      usbphy_probe,
    .disconnect = usbphy_disconnect,
};

static int usbphy_open(struct inode *, struct file *);
static int usbphy_release(struct inode *, struct file *);
static ssize_t usbphy_read(struct file *, char *, size_t, loff_t *);
static ssize_t usbphy_write(struct file *, const char *, size_t, loff_t *);
static long usbphy_ioctl(struct file *, unsigned int, unsigned long);

static const struct file_operations usbphy_fops = {
    .owner =          THIS_MODULE,
    .read =           usbphy_read,
    .write =          usbphy_write,
    .unlocked_ioctl = usbphy_ioctl,
    .open =           usbphy_open,
    .release =        usbphy_release,
    .llseek =         noop_llseek,
};
 
// SDEP Address Translation Table
static sdepAddressTranslation_f SAT[MAX_ENTRIES];

/**
 * @brief    The initialization function of the module called at insmod
 * @param    None
 * @retval   positive number if success else a negative number if error
 */
static int __init usbphy_init(void)
{
    printk(KERN_INFO USB_SDEP_DNAME ": -------------------------------------------\n");
    printk(KERN_INFO USB_SDEP_DNAME ": Initializing the SDEP LKM version: " SDEPUSBVERSION "\n");
    printk(KERN_INFO USB_SDEP_DNAME ": -------------------------------------------\n");
    
    initializeSAT(SAT); // Initialize SAT table

    // Try to dynamically allocate a major number for the device -- more difficult but worth it
    majorNumber = register_chrdev(0, USB_SDEP_DNAME, &usbphy_fops);
    if (majorNumber<0){
        printk(KERN_ALERT USB_SDEP_DNAME " failed to register a major number\n");
        return majorNumber;
    }
    printk(KERN_INFO USB_SDEP_DNAME ": registered correctly with major number %d\n", majorNumber);

    // Register the device class
    sdepUSBClass = class_create(THIS_MODULE, USB_SDEP_CNAME);
    if (IS_ERR(sdepUSBClass)){                 // Check for error and clean up if there is
        unregister_chrdev(majorNumber, USB_SDEP_DNAME);
        printk(KERN_ALERT "Failed to register device class\n");
        return PTR_ERR(sdepUSBClass);          // Correct way to return an error on a pointer
    }
    printk(KERN_INFO USB_SDEP_DNAME ": device class registered correctly\n");

    // Register the device driver
    sdepUSBDevice = device_create(sdepUSBClass, NULL, MKDEV(majorNumber, 0), NULL, USB_SDEP_DNAME);
    if (IS_ERR(sdepUSBDevice)){                // Clean up if there is an error
        class_destroy(sdepUSBClass);           // Repeated code but the alternative is goto statements
        unregister_chrdev(majorNumber, USB_SDEP_DNAME);
        printk(KERN_ALERT USB_SDEP_DNAME " failed to create the device\n");
        return PTR_ERR(sdepUSBDevice);
    }
    
    printk(KERN_INFO USB_SDEP_DNAME ": device created correctly\n");
    printk(KERN_INFO USB_SDEP_DNAME ": device class created correctly\n");
    
    // Starting the timer for checkAlive sub-process
    printk(KERN_INFO USB_SDEP_DNAME ": initialize periodic checkAlive sub-process\n");
    setup_timer(&ayaTimer, ayaTimerHandler, 0);
    if (ayaInterval>0) {
        mod_timer( &ayaTimer, jiffies + msecs_to_jiffies(ayaInterval*AYABASETIME));
        printk(KERN_INFO USB_SDEP_DNAME ": periodic checkAlive sub-process initialized\n");
    } else {
        printk(KERN_INFO USB_SDEP_DNAME ": periodic checkAlive sub-process not installed\n");
    }
    
    if ((numAyaTimedouts<1) && (numAyaTimedouts>255)) {
        numAyaTimedouts = 10;
        printk(KERN_ALERT USB_SDEP_DNAME ": incorrect value for numAyaTimedouts: reset to default 10\n");
    }
    
    memset(intQueue, BADDRESS, MAX_ENTRIES);
    
    printk(KERN_INFO USB_SDEP_DNAME ": register the driver to the usb-core\n");
    return usb_register(&usbphy_driver);
}
 
/**
 * @brief    The deinitialization function of the module called at rmmod
 * @param    None
 * @retval   positive number if success else negative number if error
 */
static void __exit usbphy_exit(void)
{
    usb_deregister(&usbphy_driver);
    printk(KERN_INFO USB_SDEP_DNAME ": deregister the driver from the usb-core\n");
  
    del_timer(&ayaTimer);
     printk(KERN_INFO USB_SDEP_DNAME ": periodic checkAlive sub-process killed\n");
   
    device_destroy(sdepUSBClass, MKDEV(majorNumber, 0));     // remove the device
    class_unregister(sdepUSBClass);                          // unregister the device class
    class_destroy(sdepUSBClass);                             // remove the device class
    unregister_chrdev(majorNumber, USB_SDEP_DNAME);          // unregister the major number
    
    printk(KERN_INFO USB_SDEP_DNAME ": device and device class destroyed correctly\n");
    printk(KERN_INFO USB_SDEP_DNAME ": -------------------------------------\n");
    printk(KERN_INFO USB_SDEP_DNAME ": Unload the SDEP LKM version: " SDEPUSBVERSION "\n");
    printk(KERN_INFO USB_SDEP_DNAME ": -------------------------------------\n");
}

/**
 * @brief    The probing function of the usb module called from the usb-core at plug-in of mathed usbdev
 * @param    interface - allocated interface for the usbdev from usb-core
 * @param    id - mached table for this device
 * @retval   zero if accept the usbdev else a negative number if error
 */
static int usbphy_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
    sdepUSBPHY_t *sdepUSBPHY;
    struct usb_host_interface *iface_desc;
    struct usb_endpoint_descriptor *endpoint;
    uint32_t pSerial;
    uint32_t pSDEPVer;    
    uint8_t pAddress; 
    int8_t retVal;
    int i;
    
    iface_desc = interface->cur_altsetting;
    if(iface_desc->desc.bInterfaceClass==USB_SDEP_IF_CLASS){
        connectedDEV++;
        sdepUSBPHY = (sdepUSBPHY_t*)kzalloc(sizeof(sdepUSBPHY_t), GFP_KERNEL);
        
        sdepUSBPHY->flags.interrupted = 0;
        sdepUSBPHY->flags.syncTrans = 0;
        sdepUSBPHY->flags.asyncTrans = 0;

        sdepUSBPHY->countedTimeout = 0;

        sdepUSBPHY->udev = usb_get_dev(interface_to_usbdev(interface));
        sdepUSBPHY->interface = interface;
        for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {
            endpoint = &iface_desc->endpoint[i].desc;

            if (!sdepUSBPHY->bulk_in_endpointAddr && (endpoint->bEndpointAddress & USB_DIR_IN) && ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)) {
                /* we found a bulk in endpoint */
                sdepUSBPHY->bulk_in_endpointAddr = endpoint->bEndpointAddress;
            }

            if (!sdepUSBPHY->bulk_out_endpointAddr && !(endpoint->bEndpointAddress & USB_DIR_IN) && ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)) {
                /* we found a bulk out endpoint */
                sdepUSBPHY->bulk_out_endpointAddr = endpoint->bEndpointAddress;
            }
        }
        if (!(sdepUSBPHY->bulk_in_endpointAddr && sdepUSBPHY->bulk_out_endpointAddr)) {
            printk(KERN_ALERT USB_SDEP_DNAME ": could not find both bulk-in and bulk-out endpoints");
            kfree(sdepUSBPHY);
            return -ENODEV;
        }

        // Recovery the PROBE serial number
        // If it's impossible to recover the serial block the accept of USB device
        if (testGetSerial(sdepUSBPHY, &pSDEPVer, &pSerial) != SUCCESS) {
            connectedDEV--;
            kfree(sdepUSBPHY);
            return -ENODEV;
        }

        // Recovery a free address
        retVal = getFreeAddress(SAT, &pAddress);
        // If there isn't a free address or device not accept its address block the accept of USB device
        if ((retVal==-LISTISFULL) || (setAddress(sdepUSBPHY, pAddress) != SUCCESS)) {
            connectedDEV--;
            kfree(sdepUSBPHY);
            return -ENODEV;
        }

        sdepUSBPHY->dAddress=pAddress;
        spin_lock_init(&sdepUSBPHY->sLock);
        
        SAT[pAddress].pAddress=pAddress;
        SAT[pAddress].pSDEPVer=pSDEPVer;
        SAT[pAddress].pSerial=pSerial;
        SAT[pAddress].pStatus=S_RUNNING;
        SAT[pAddress].sdepUSB=sdepUSBPHY;

        usb_get_intf(interface);
        
        // Trigger the connected variation event
        if (currentPPID) send_signal(currentPPID, SIGLISTCHANGE); 

        dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE drive (%04X:%04X) with serial %08x plugged-in and assigned at %d address\n", id->idVendor, id->idProduct, pSerial, pAddress);
        dprintk(KERN_INFO USB_SDEP_DNAME ": now are connected %d PROBEs\n", connectedDEV);
        return SUCCESS;
    }
   
    return -ENODEV;
}

/**
 * @brief    The disconnect function of the usb module called from the usb-core at plug-out of mathed usbdev
 * @param    interface - allocated interface for the usbdev from usb-core
 * @retval   None
 */
static void usbphy_disconnect(struct usb_interface *interface)
{
    sdepUSBPHY_t *sdepUSBPHY;
    int8_t retVal;
    uint8_t pAddress; 
    
    retVal = addressFromInterface(SAT, interface, &pAddress);
    
    if (retVal == SUCCESS) {
        dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE drive with %d address are plugged-out\n", pAddress);
        sdepUSBPHY=SAT[pAddress].sdepUSB;
        usbphy_softdisconnect(sdepUSBPHY);
    }

    usb_driver_release_interface(&usbphy_driver, interface);
}

/**
 * @brief    The function for the software disconnect of the probe
 * @param    sdepUSBPHY - allocated sdepUSBPHY struct
 * @retval   None
 */
static void usbphy_softdisconnect(sdepUSBPHY_t *sdepUSBPHY)
{
    uint8_t pAddress;
    
    if (sdepUSBPHY){
        pAddress = sdepUSBPHY->dAddress;
        
        if (SAT[pAddress].pAddress != BADDRESS) {
            connectedDEV--;

            if (sdepUSBPHY) {
                usb_put_intf(sdepUSBPHY->interface);
                usb_put_dev(sdepUSBPHY->udev);
                kfree(sdepUSBPHY);
            }
            
            SAT[pAddress].pAddress = BADDRESS;
            SAT[pAddress].pSerial = 0;
            SAT[pAddress].pStatus = S_DISCONNECT;
            SAT[pAddress].sdepUSB = NULL;

            // Trigger the connected variation event
            if (currentPPID) send_signal(currentPPID, SIGLISTCHANGE); 
            
            dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE at address %d are disconnected\n", pAddress);
            dprintk(KERN_INFO USB_SDEP_DNAME ": now are connected %d PROBEs\n", connectedDEV);
        }
    }
}


/**
 * @brief    The open syscall function for the character device
 * @param    inodep - associated inode
 * @param    filep - associate file pointer
 * @retval   SUCCESS if success else a negative number if error
 */
static int usbphy_open(struct inode *inodep, struct file *filep)
{
    if (openedDEV)
        return -EBUSY;

    // Increment the usage count
    openedDEV++;
    //INC_USECOUNT();
    
    // Recover the PID of process that call this syscall (current is global)
    currentPPID = current->pid;

    return SUCCESS;
}

/**
 * @brief    The close syscall function for the character device
 * @param    inodep - associated inode
 * @param    filep - associate file pointer
 * @retval   SUCCESS if success else a negative number if error
 */
static int usbphy_release(struct inode *inodep, struct file *filep)
{
    // Decrement the usage count
    openedDEV--;
    //DEC_USECOUNT();
    
    // Reset the PID process association to driver
    currentPPID = 0;

    return SUCCESS;
}

/**
 * @brief    The read syscall function for the character device
 * @param    filep - associate file pointer
 * @param    buffer - pointer to user space buffer where save the bytes
 * @param    len - number of bytes to read
 * @param    offset - offset where save
 * @retval   number of readed bytes if success else a negative number if error
 */
static ssize_t usbphy_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
    int retVal = 0;
    int count = 0;
    char *io_buffer;
    sdepUSBPHY_t *sdepUSBPHY;
    unsigned long irq_flags;


    if (SAT[currentSelected].pAddress == BADDRESS)
        return -ENODEV;

    sdepUSBPHY=SAT[currentSelected].sdepUSB;

    if (!sdepUSBPHY)
        return -ENODEV;
      
    // Check for the end transaction command
    if (len == 0) {
        spin_lock_irqsave(&sdepUSBPHY->sLock, irq_flags);
        sdepUSBPHY->flags.syncTrans = 0;
        spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);
        
        dprintk(KERN_INFO USB_SDEP_DNAME ": syncTrans - terminated\n");
        return SUCCESS;
    }

    // If is in sync transaction
    if (sdepUSBPHY->flags.syncTrans == 1){

        // Check if the user space memory area is correct
        if (access_ok(VERIFY_WRITE, buffer, len)) {

            io_buffer = kzalloc(len, GFP_KERNEL);
            if (!io_buffer) {
                printk(KERN_INFO USB_SDEP_DNAME ": can't allocate io_buffer in read");
                return -EFAULT;
            }
    
            /* do a blocking bulk read to get data from the device */
            retVal = usb_bulk_msg(sdepUSBPHY->udev, usb_rcvbulkpipe(sdepUSBPHY->udev, sdepUSBPHY->bulk_in_endpointAddr), io_buffer, len, &count, usbTimeout);

            /* if the read was successful, copy the data to user space */
            if (!retVal) {

                if (!copy_to_user(buffer, io_buffer, count))
                    retVal = count;
                else
                    retVal = -EFAULT;
            }
    
            kfree(io_buffer);

        } else {
            // If not correct no nedded and return NOMEM error
            retVal = -ENOMEM;
        }

    }else{
        // If a synct transaction is not running 
        retVal = -NOTRANS;
    }

    return retVal;
}



/**
 * @brief    The write syscall function for the character device
 * @param    filep - associate file pointer
 * @param    buffer - pointer to user space buffer where save the bytes
 * @param    len - number of bytes to read
 * @param    offset - offset where save
 * @retval   number of writed bytes if success else a negative number if error
 */
static ssize_t usbphy_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
    int retVal = 0;
    int count = 0;
    char *io_buffer;
    sdepUSBPHY_t *sdepUSBPHY;
    unsigned long irq_flags;

    if (SAT[currentSelected].pAddress == BADDRESS)
        return -ENODEV;

    sdepUSBPHY=SAT[currentSelected].sdepUSB;

    if (!sdepUSBPHY)
        return -ENODEV;

    // Check for the start transaction command
    if (len == 0) {
        // Check if a previous transaction is in timeout and terminate it
        spin_lock_irqsave(&sdepUSBPHY->sLock, irq_flags);
        if (jiffies_to_msecs(jiffies-sdepUSBPHY->transSTime) > transTimedout){
            if (sdepUSBPHY->flags.syncTrans == 1){
                sdepUSBPHY->flags.syncTrans = 0;
            }
            if (sdepUSBPHY->flags.asyncTrans == 1){
                sdepUSBPHY->flags.asyncTrans = 0;
                usb_unlink_urb(sdepUSBPHY->transfer_urb);
            }      
        }
        spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);  
        
        // Check if the channel is empty or an asynctTrans is running
        spin_lock_irqsave(&sdepUSBPHY->sLock, irq_flags);
        if (sdepUSBPHY->flags.asyncTrans == 0){
            // If free acquire the channel and start transaction
            sdepUSBPHY->transSTime = jiffies;
            sdepUSBPHY->flags.syncTrans = 1;
            spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);
            
            dprintk(KERN_INFO USB_SDEP_DNAME ": syncTrans - started\n");
            return SUCCESS;
        } else {
            spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);
            return -EBUSY;
        }
    }
    if(sdepUSBPHY->flags.syncTrans == 1){
        // Check if the user space memory area is correct
        if (access_ok(VERIFY_READ, buffer, len)) {

            io_buffer = kzalloc(len, GFP_KERNEL);
            if (!io_buffer) {
                printk(KERN_ALERT USB_SDEP_DNAME ": can't allocate io_buffer write");
                return -EFAULT;
            }

            copy_from_user(io_buffer, buffer, len);
            /* do a blocking bulk write to put data to the device */
            retVal = usb_bulk_msg(sdepUSBPHY->udev, usb_sndbulkpipe(sdepUSBPHY->udev, sdepUSBPHY->bulk_out_endpointAddr), io_buffer, len, &count, usbTimeout);
            
            if (!retVal) {
                retVal = count;
            } else {
                retVal = -EFAULT;
            }

            kfree(io_buffer);

        } else {
            // If not correct no nedded and return NOMEM error
            retVal = -ENOMEM;
        }
    }else{
        // If not possible start a sync transaction 
        retVal = -EBUSY;
    }
    
    return retVal;
}

/**
 * @brief    The ioctl syscall function for the character device for control the driver
 * @param    filep - associate file pointer
 * @param    ioctl_num - special ioctl command number
 * @param    ioctl_param - passed ioctl parameter (can be anything, also a pointer)
 * @retval   depend from ioctl function if success, else a negative number if error
 */
static long usbphy_ioctl(struct file *filep, unsigned int ioctl_num, unsigned long ioctl_param)
{
    sdepUSBPHY_t *sdepUSBPHY;
    sdepAddressList_f *alist_buffer;
    int passedSelected = 0;
    uint8_t s_i = 0, a_i = 0;
    uint8_t pAddress;
    long retVal = -EFAULT;
    
    switch(ioctl_num){
        /**
         * @brief  This ioctl function return the number of attached device
         * @param  ioctl_num - IOCTL_HOWMANYDEVICE
         * @param  ioctl_param - None
         * @retval The number of attached device
         */
        case IOCTL_HOWMANYDEVICE:
            retVal = connectedDEV;
            break;
        /**
         * @brief  This ioctl function return the list of attached device with address and serial
         * @param  ioctl_num - IOCTL_GETLIST
         * @param  ioctl_param - address of a correct user space allocated memory where copy the list
         * @retval The number of attached device if a correct user space pointer is passed else -ENOMEM
         */
        case IOCTL_GETLIST:
            // Allocate the new temporary buffer for the attached list
            alist_buffer = (sdepAddressList_f*)kzalloc(sizeof(sdepAddressList_f)*MAX_ENTRIES, GFP_KERNEL);

            for (s_i=0, a_i=0; s_i<MAX_ENTRIES; s_i++){
                // Copy in the list all attached device
                if (SAT[s_i].pAddress != BADDRESS){
                    alist_buffer[a_i].pAddress=SAT[s_i].pAddress;
                    alist_buffer[a_i].pSDEPVer=SAT[s_i].pSDEPVer;
                    alist_buffer[a_i].pSerial=SAT[s_i].pSerial;
                    a_i++;
                }
            }
    
            // Check if the user space memory area is correct
            if (access_ok(VERIFY_WRITE, (char*)ioctl_param, (sizeof(sdepAddressList_f)*a_i))) {
                copy_to_user((char*)ioctl_param, alist_buffer, (sizeof(sdepAddressList_f)*a_i));
                retVal = a_i;
            } else {
                // If not correct no nedded and return NOMEM error
                retVal = -ENOMEM;
            }  
     
            kfree(alist_buffer);
            break;
    
        /**
         * @brief  This ioctl function select the device where read and write the message
         * @param  ioctl_num - IOCTL_SELECT
         * @param  ioctl_param - address of device to select for transmission
         * @retval SUCCESS if success else -ENODEV if selected a unattached device
         */
        case IOCTL_SELECT:
            passedSelected = ioctl_param;
    
            if ((passedSelected>=0) && (passedSelected<MAX_ENTRIES)) {
                if (SAT[passedSelected].pAddress != BADDRESS) {
                    currentSelected = passedSelected;
                    retVal = SUCCESS;
                } else {
                    retVal = -ENODEV;
                }
            }
            break;
    
        /**
         * @brief  This ioctl function pop a address from the interrupted queue
         * @param  ioctl_num - IOCTL_POPNEXTINT
         * @param  ioctl_param - address of the correct user space memory where copy the pAddress
         * @retval SUCCESS if success else -ENOMEM if an incorrect user space address is passed
         */
        case IOCTL_POPNEXTINT:
            pAddress = popInt(intQueue);
            if (pAddress != BADDRESS) {
                if (access_ok(VERIFY_WRITE, (char*)ioctl_param, (sizeof(sdepAddressList_f)*a_i))) {
                    copy_to_user((char*)ioctl_param, &pAddress, sizeof(pAddress));
                    retVal = SUCCESS;
                } else {
                    // If not correct no nedded and return NOMEM error
                    retVal = -ENOMEM;
                }  
            } else {
                retVal = -ENODEV;
            }
            break;

        /**
         * @brief  This ioctl function force the disconnect of a device
         * @param  ioctl_num - IOCTL_DISCONNECT
         * @param  ioctl_param - address of device to disconnect
         * @retval SUCCESS if success else -ENODEV if the device is not connected
         */
        case IOCTL_DISCONNECT:
            passedSelected = ioctl_param;
    
            if (SAT[passedSelected].pAddress != BADDRESS){

                sdepUSBPHY=SAT[passedSelected].sdepUSB;
    
                if (sendDisconnect(sdepUSBPHY, passedSelected) == SUCCESS){
                    dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE at address %d are disconnected from HUB\n", passedSelected);
    
                    if (sdepUSBPHY) {
                        usbphy_softdisconnect(sdepUSBPHY);
                    }
    
                    retVal = SUCCESS;
                }
            } else {
                retVal = -ENODEV;
            }
            break;
    
        default:
            break;
    }
    
    return retVal;
}




/**
 * @brief    Execute a full checkAlive cycle on all connected device
 * @param    data - passed data from the callback timer
 * @retval   None
 */
void ayaTimerHandler(unsigned long data)
{    
    uint8_t i;
    sdepUSBPHY_t *sdepUSBPHY;
    unsigned long irq_flags;
    
    uint8_t aliveMessage[] = "\x00\xFF\x00\x00";
    uint32_t ioBytes;
    
    aliveMessage[0]=AREYOUALIVE_mType;

    if ((numAyaTimedouts<1) && (numAyaTimedouts>255)) {
        numAyaTimedouts = 10;
        printk(KERN_ALERT USB_SDEP_DNAME ": incorrect value for numAyaTimedouts: reset to default 10\n");
    }
    
    for (i=0; i<MAX_ENTRIES; i++){
        if ((SAT[i].pStatus == S_RUNNING) && (SAT[i].pAddress != BADDRESS)){

            sdepUSBPHY=SAT[i].sdepUSB;
            
            if (sdepUSBPHY) {
 
                // Check if the previous transaction on this probe are in timeout
                if (jiffies_to_msecs(jiffies-sdepUSBPHY->transSTime) > transTimedout){
                    // Check the sync transaction and terminate it
                    spin_lock_irqsave(&sdepUSBPHY->sLock, irq_flags);
                    if (sdepUSBPHY->flags.syncTrans == 1){
                        sdepUSBPHY->flags.syncTrans = 0;
                    }
                    spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);     
                    
                    // Check the async transaction and count number of timeout
                    // If this number is major of numAyaTimedouts disconnect the probe
                    spin_lock_irqsave(&sdepUSBPHY->sLock, irq_flags);
                    if (sdepUSBPHY->flags.asyncTrans == 1){
                        spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);     
                        sdepUSBPHY->countedTimeout += 1;

                        if (sdepUSBPHY->countedTimeout <= numAyaTimedouts) {
                            dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE at address %d - %d/%d timeout\n", sdepUSBPHY->dAddress, sdepUSBPHY->countedTimeout, numAyaTimedouts);
                        } else {
                            if (sdepUSBPHY->countedTimeout >= numAyaTimedouts) {
                                dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE at address %d are dead\n", sdepUSBPHY->dAddress);
                                usbphy_softdisconnect(sdepUSBPHY);
                            } 
                        }
                    } else {
                        spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);
                    }
                }

                if (sdepUSBPHY->flags.asyncTrans == 0){
                    // Check if the channel is free or a synctTrans is running
                    spin_lock_irqsave(&sdepUSBPHY->sLock, irq_flags);
                    if (sdepUSBPHY->flags.syncTrans == 0){
                        // If free acquire the channel and start transaction
                        sdepUSBPHY->transSTime = jiffies;
                        sdepUSBPHY->flags.asyncTrans = 1;
                        spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);
                        
                        dprintk(KERN_INFO USB_SDEP_DNAME ": asyncTrans - started\n");
                        dprintk(KERN_INFO USB_SDEP_DNAME ": check on PROBE at address %d\n", i);

                        aliveMessage[1]=i;
            
                        sdepUSBPHY->transfer_buffer = (uint8_t*)kzalloc(sizeof(aliveMessage), GFP_ATOMIC);
                        if (!sdepUSBPHY->transfer_buffer) {
                               printk(KERN_ALERT USB_SDEP_DNAME ": can't allocate rcvBuffer");
                        }
            
                        memcpy(sdepUSBPHY->transfer_buffer, aliveMessage, sizeof(aliveMessage));
                        ioBytes = usbphy_bulkrw_async(sdepUSBPHY, BULK_OUT, sdepUSBPHY->transfer_buffer, 4, usbphy_ayatxcomplete);
                        
                        dprintk(KERN_INFO USB_SDEP_DNAME ": chekAlive - 1st stage run\n");
                    } else {
                        spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);
                    }
                }
            }
        }
    }

    // Restarting the sub-process timer
    if (ayaInterval>0) {
        mod_timer(&ayaTimer, jiffies + msecs_to_jiffies(ayaInterval*AYABASETIME));
    }

}

/**
 * @brief    Second stage of checkAlive cycle on a specific device for init a async receive of ALIVEACK
 * @param    data - passed data from the callback timer
 * @retval   None
 */
void usbphy_ayatxcomplete(struct urb *urb){
    uint32_t ioBytes;
    sdepUSBPHY_t *sdepUSBPHY;
    
    sdepUSBPHY = (sdepUSBPHY_t*)urb->context;

    usb_free_urb(urb);
    
    if ((SAT[sdepUSBPHY->dAddress].sdepUSB) && (SAT[sdepUSBPHY->dAddress].pAddress != BADDRESS)){
    
        if (sdepUSBPHY->transfer_buffer) kfree(sdepUSBPHY->transfer_buffer);
        sdepUSBPHY->transfer_buffer = (uint8_t*)kzalloc(4, GFP_ATOMIC);
        if (!sdepUSBPHY->transfer_buffer) {
            printk(KERN_ALERT USB_SDEP_DNAME ": can't allocate rcvBuffer");
        }

        ioBytes = usbphy_bulkrw_async(sdepUSBPHY, BULK_IN, sdepUSBPHY->transfer_buffer, 4, usbphy_ayarxcomplete);
        dprintk(KERN_INFO USB_SDEP_DNAME ": chekAlive - 2nd stage run\n");
        
    }
}

/**
 * @brief    Execute a full checkAlive cycle on all connected device
 * @param    data - passed data from the callback timer
 * @retval   None
 */
void usbphy_ayarxcomplete(struct urb *urb){
    sdepUSBPHY_t *sdepUSBPHY;
    unsigned long irq_flags;
   
    sdepUSBPHY = (sdepUSBPHY_t*)urb->context;
    
    if ((SAT[sdepUSBPHY->dAddress].sdepUSB) && (SAT[sdepUSBPHY->dAddress].pAddress != BADDRESS)) {
    
        // At and of asyncTrans release the channel
        spin_lock_irqsave(&sdepUSBPHY->sLock, irq_flags);
        sdepUSBPHY->flags.asyncTrans = 0;
        spin_unlock_irqrestore(&sdepUSBPHY->sLock, irq_flags);

        dprintk(KERN_INFO USB_SDEP_DNAME ": chekAlive - 3nd stage run\n");
        dprintk(KERN_INFO USB_SDEP_DNAME ": asyncTrans - terminated\n");

        if (((*((uint8_t*)urb->transfer_buffer) & mType_MASK) == ALIVEACK_mType) && (*((uint8_t*)urb->transfer_buffer+1) == sdepUSBPHY->dAddress)) {
            if (GETBIT(*((uint8_t*)urb->transfer_buffer), 7) == 1) {
                if (sdepUSBPHY->flags.interrupted == 0) {
                    if (currentPPID) {
                        // Disable future interrupted triggering from this probe
                        // sdepUSBPHY->flags.interrupted = 1;
                        // Push in the queue the address of interrupted device
                        pushInt(intQueue, sdepUSBPHY->dAddress);
                        // Trigger the software interrupt event
                        send_signal(currentPPID, SIGDEVINTERRUPT);
                    }
                } 
            } else {
                // Renable future interrupted triggering from this probe
                sdepUSBPHY->flags.interrupted = 0;
            }
            
            sdepUSBPHY->countedTimeout = 0;
           
            dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE at address %d are alive\n", sdepUSBPHY->dAddress);
        } 

        if (sdepUSBPHY->transfer_buffer) kfree(sdepUSBPHY->transfer_buffer);
        
    }
    
    usb_free_urb(urb);
    
}

module_init(usbphy_init);
module_exit(usbphy_exit);
