/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "sdepCore.h"
#include "sdepPHY.h"

void sig_handler(int sig_num);

// Protocol callbacks
int8_t ackResponse(mAddress_t pAddress);
int8_t errorResponse(mAddress_t pAddress, sError_t sErrorList[], uint16_t seNum);
int8_t errorResponseInt (mAddress_t pAddress);
int8_t capabilityResponse(mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum);
int8_t dataArriveResponse(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize);
int8_t customResponse(mAddress_t pAddress, mPayload_t *mPayload, pLength_t pLength);
int8_t dataArriveResponseInt (mAddress_t pAddress, sId_t sensorId, uint8_t intType, uint8_t samplesArray[], uint16_t arrSize);

int main(int argc, char *argv[]) {
	uint32_t i;
	int32_t retVal;
	uint32_t attachedNum;
	
	uint32_t currTest = 0;
	uint32_t numOfTest = 10;
	uint32_t errorCount = 0;

	sdepAddressList_f *attachedList=NULL;//(void*)0;
	
	// --- BEGIN Test packets ---
	sConfig_t sConfigList[] = {
		{
			.sensorId = {
				.sensorType = 1,
				.sensorNum = 0,
			},
			.sensorMode.reqSettings=ONESHOTMode,
			.sensorMode.sActive = 1,
			.sensorMode.intSetting = 0,
			.sensorConfig = 0,		
		},
		{
			.sensorId = {
				.sensorType = 2,
				.sensorNum = 0,
			},
			.sensorMode.reqSettings=ONESHOTMode,
			.sensorMode.sActive = 1,
			.sensorMode.intSetting = 0,
			.sensorConfig = 0,		
		},
	};
	uint16_t scNum = sizeof(sConfigList)/sizeof(sConfig_t);
	
	sId_t sensorId = {
		.sensorType = 2,
		.sensorNum = 0,	
	};
	
	mPayload_t mPayload[] = {0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00,  0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00,  0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00,  0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00, 0x00, 0x05, 0x0F, 0xFF, 0xF0, 0x50, 0x00};
	pLength_t pLength = sizeof(mPayload)/sizeof(mPayload[0]);
	// --- END Test packets ---

	// Initialize the callbacks
	phyDriver_t phyDriver = {
		.selectAddress = selectAddress,
		.writeBytes = writeChars,
		.readBytes = readChars
	};
	
	sdepCallbacks_t sdepCallbacks = {
		.ackResponse = ackResponse,
		.errorResponse = errorResponse,
		.errorResponseInt = errorResponseInt,
		.capabilityResponse = capabilityResponse,
		.dataArriveResponse = dataArriveResponse,
		.dataArriveResponseInt = dataArriveResponseInt,
		.customResponse = customResponse
	};

	retVal = openDriver();
	if (retVal != SUCCESS){
		if (retVal == -EPERM) {
			printf("Error opening /dev/sdepUSB file driver. Check your permissions.\n");
		}
		if (retVal == -EBUSY) {
			printf("Error opening /dev/sdepUSB file driver. Already in use.\n");
		}
		return -1;
	}
	
	if (getAttachedList(&attachedList, &attachedNum) == SUCCESS) {
	
		printf("Attached device list:\n");
		for (i=0; i<attachedNum; i++){
			printf("SDEP Device[%d] at address %d with serial %08x\n",i, attachedList[i].pAddress, attachedList[i].pSerial);
		}
		
		registerSignalHandler(SIGLISTCHANGE, sig_handler);
		registerSignalHandler(SIGDEVINTERRUPT, sig_handler);
	
		// Initialize the SDEP core
		sdepInitCore(phyDriver, sdepCallbacks);
        
        currTest=0;
		printf("Test ONE-SHOT:\n");
        // Setting ONE-SHOT
        for (i=0; i<attachedNum; i++){
			if (sendCapabilityRequest(attachedList[i].pAddress) < 0) errorCount++;
			if (sendConfigTo(attachedList[i].pAddress, sConfigList, scNum) < 0) errorCount++;
		}
		// Start the test on all machine
		while(currTest++ < numOfTest) {
			for (i=0; i<attachedNum; i++){
			// Device XX - Test
				printf("\nSend the samples packet to address %x:\n", attachedList[i].pAddress); 
				if (sendCustom(attachedList[i].pAddress, mPayload, pLength) < 0) errorCount++;
				sensorId.sensorType=1;
				if (sendSamplesRequest(attachedList[i].pAddress, sensorId) < 0) errorCount++;
				sensorId.sensorType=2;
				if (sendSamplesRequest(attachedList[i].pAddress, sensorId) < 0) errorCount++;
			}
		}
		printf("Test executed %d times with %d numbers of errors\n", numOfTest, errorCount);

        currTest=0;
		printf("Test PERIODIC-MODE no INTERRUPT:\n");
		// Setting PERIODIC-MODE
		sConfigList[0].sensorMode.reqSettings=PERIODICMode;
		sConfigList[0].sensorConfig = 100;
		sConfigList[1].sensorMode.reqSettings=PERIODICMode;
		sConfigList[1].sensorConfig = 250;
        for (i=0; i<attachedNum; i++){
			if (sendConfigTo(attachedList[i].pAddress, sConfigList, scNum) < 0) errorCount++;
		}
		// Start the test on all machine
		while(currTest++ < numOfTest) {
			for (i=0; i<attachedNum; i++){
				sleep(3);
				sensorId.sensorType=1;
				if (sendSamplesRequest(attachedList[i].pAddress, sensorId) < 0) errorCount++;
				sensorId.sensorType=2;
				if (sendSamplesRequest(attachedList[i].pAddress, sensorId) < 0) errorCount++;
			}
		}
		printf("Test executed %d times with %d numbers of errors\n", numOfTest, errorCount);
		
		printf("Test PERIODIC-MODE with INTERRUPT:\n");
		// Setting PERIODIC-MODE with INTERRUPT
		sConfigList[0].sensorMode.intSetting = 1;
		sConfigList[1].sensorMode.intSetting = 1;
		for (i=0; i<attachedNum; i++){
			if (sendConfigTo(attachedList[i].pAddress, sConfigList, scNum) < 0) errorCount++;
		}
		while(1) {}
	}
	
	freeAttachedList(attachedList);
	
	closeDriver();
}


// --------------------------------------------------------
// ------------ NEW INTERRUPT FUNCTIONALITY ---------------
// --------------------------------------------------------

// Handle function for SIGNAL triggering
void sig_handler(int sig_num){
	uint8_t pAddress, i;
	uint32_t attachedNum;
	sdepAddressList_f *attachedList=NULL;
	
	// Case VARIATION IN ATTACHED LIST
	if (sig_num == SIGLISTCHANGE) {
		printf("SIGNAL: You have received an ATTACHED LIST CHANGE signal.\n");
		if (getAttachedList(&attachedList, &attachedNum) == SUCCESS) {
			printf("UPDATED: Attached device list:\n");
			for (i=0; i<attachedNum; i++){
				printf("SDEP Device[%d] at address %d with serial %08x\n",i, attachedList[i].pAddress, attachedList[i].pSerial);
			}
			printf("\n");
		}
		freeAttachedList(attachedList);
	}
	
	// Case INTERRUPT
	if (sig_num == SIGDEVINTERRUPT) {
			popNextInterrupted(&pAddress);
			printf("SIGNAL: You have received an INTERRUPT signal from %x.\n", pAddress);
			sendSamplesRequestInt(pAddress);
	}
	printf("\n");
	
}
// --------------------------------------------------------
// --------------------------------------------------------
// --------------------------------------------------------


int8_t ackResponse(mAddress_t pAddress){
	printf("RESPONSE: You have received a ACK message from %x.\n", pAddress);
	printf("\n");

	return sdepOK;
}

int8_t errorResponse(mAddress_t pAddress, sError_t sErrorList[], uint16_t seNum){
	int i;
	
	printf("RESPONSE: You have received a ERROR message from %x of len %d:\n", pAddress, seNum);
	for (i=0; i<seNum; i++){
		printf("sErrorList[%d].sensorId.sensorType = %d\n", i, sErrorList[i].sensorId.sensorType);
		printf("sErrorList[%d].sensorId.sensorNum = %d\n", i, sErrorList[i].sensorId.sensorNum);
		printf("sErrorList[%d].errorCode = %d\n", i, sErrorList[i].errorCode);
	}
	printf("\n");
	
	return sdepOK;
}

int8_t capabilityResponse(mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum){
	int i;
	
	printf("RESPONSE: You have received a MYCAP message from %x of len %d:\n", pAddress, scNum);
	for (i=0; i<scNum; i++){
		printf("sCapsList[%d].sensorType = %d\n", i, sCapsList[i].sensorType);
		printf("sCapsList[%d].sensorQuantity = %d\n", i, sCapsList[i].sensorQuantity);
		printf("sCapsList[%d].sensorCap = {\n", i);
		printf("\t.intSupport = %d\n", sCapsList[i].sensorCap.intSupport);
		printf("\t.reqSupport = %d\n", sCapsList[i].sensorCap.reqSupport);
		printf("\t.sampleLen = %d\n}\n", sCapsList[i].sensorCap.sampleLen);
	}
	printf("\n");
	
	return sdepOK;
}

int8_t dataArriveResponse(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize){
	int i;
	int16_t x,y,z;
	printf("RESPONSE: You have received a DEXG message from %x of size %d:\n", pAddress, arrSize);
	printf("sensorId.sensorType = %d\n", sensorId.sensorType);
	printf("sensorId.sensorNum = %d\n", sensorId.sensorNum);
	//printf("sample:%d %d %d\n",*((int16_t*)&samplesArray[0]),*((int16_t*)&samplesArray[2]),*((int16_t*)&samplesArray[4]));
	switch(sensorId.sensorType){
		case 1:
			for (i=0; i<(arrSize/6); i++){
				x=*((int16_t*)&(samplesArray[i*6]));
				y=*((int16_t*)&(samplesArray[(i*6)+2]));
				z=*((int16_t*)&(samplesArray[(i*6)+4]));
				printf("X[%d]:%d\t\tY[%d]:%d\t\tZ[%d]:%d\n", i, x,i, y,i, z);
			}
			break;
		case 2:
			for (i=0; i<arrSize; i++)
				printf("samplesArray[%d] = %d\n", i, ((int8_t)samplesArray[i])+25);
			break;
		default:
			break;
	}
			printf("\n");
	return sdepOK;
}

int8_t customResponse(mAddress_t pAddress, mPayload_t *mPayload, pLength_t pLength){
	int i;
	
	printf("RESPONSE: You have received a CUSTOM message from %x of size %d:\n", pAddress, pLength);
	printf("mPayload = ");
	for (i=0; i<pLength; i++){
		printf("[%d]:%x ", i, mPayload[i]);
	}
	printf("\n");
	
	return sdepOK;
}


int8_t dataArriveResponseInt (mAddress_t pAddress, sId_t sensorId, uint8_t intType, uint8_t samplesArray[], uint16_t arrSize){
int i;
int16_t x,y,z;
	printf("RESPONSE: You have received a DEXGIT message from %x of size %d:\n", pAddress, arrSize);
	printf("sensorId.sensorType = %d\n", sensorId.sensorType);
	printf("sensorId.sensorNum = %d\n", sensorId.sensorNum);
	printf("intType = %d \n", intType);
	if(arrSize>0){
		switch(sensorId.sensorType){
			case 1:
				for (i=0; i<(arrSize/6); i++){
					x=*((int16_t*)&(samplesArray[i*6]));
					y=*((int16_t*)&(samplesArray[(i*6)+2]));
					z=*((int16_t*)&(samplesArray[(i*6)+4]));
					printf("X[%d]:%d\t\tY[%d]:%d\t\tZ[%d]:%d\n", i, x,i, y,i, z);
				}
				break;
			case 2:
				for (i=0; i<arrSize; i++)
					printf("samplesArray[%d] = %d\n", i, ((int8_t)samplesArray[i])+25);
				break;
			default:
				break;
		}
		printf("\n");
	}
	return sdepOK;

}

int8_t errorResponseInt (mAddress_t pAddress){
	printf("RESPONSE: You have received a ERRORIT message from %x\n", pAddress);
	
	return sdepOK;
}

