##
## @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
##
## @author:  Castello Oscar
##           Iorio Raffaele
##
## @file:    Makefile
##
## @date:    18 July 2016
##
## @license: This work is licensed under the Creative Commons
##           Attribution-NonCommercial-ShareAlike 4.0 International License.
##           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
##

# Internal define
CC = gcc
AR = ar
SRC = $(wildcard *.c)
HDR = $(wildcard ./include/*.h)
OBJ = $(SRC:.c=.o)
LIBNAME = libsdep

# Setting flags
ARFLAGS := -rcs
CFLAGS := -I./include -c

all: $(OBJ)
	@echo " [AR] Create library "$(LIBNAME)".a from:" $?
	@$(AR) $(ARFLAGS) ../$(LIBNAME).a $?

debug: CFLAGS += -ggdb
debug: all

%.o: %.c $(HDR)
	@echo " [CC] Building "$@" from:" $<
	@$(CC) $(CFLAGS) -o $@ $<

clean:
	@echo " [CLEAN] Delete all object files "
	-@rm -rf *.o ../$(LIBNAME).a
