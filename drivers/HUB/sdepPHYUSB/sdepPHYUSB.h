/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @subproject: SDEP USB Physical Driver v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepUSBPHY.h
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#ifndef SDEP_PHY_USB
#define SDEP_PHY_USB

#include <linux/usb.h>            // Required for interface to usb_core
#include <linux/spinlock.h>       // Required for spinlock functionality

#define BADDRESS     0xFF
#define MAX_ENTRIES  128

/* SDEP Protocol Define */
#define    NULL_mType           0x00
#define    WHO_mType            0x01
#define    IAM_mType            0x02
#define    YOUARE_mType         0x03
#define    ACK_mType            0x04
#define    AREYOUALIVE_mType    0x05
#define    ALIVEACK_mType       0x06
#define    BYEBYE_mType         0x07
#define    WHAT_mType           0x08
#define    MYCAP_mType          0x09
#define    CONF_mType           0x0A
#define    DREQ_mType           0x0B
#define    DEXG_mType           0x0C
#define    ERROR_mType          0x0D
#define    DREQIT_mType         0x0E
#define    DEXGIT_mType         0x0F
#define    ERRORIT_mType        0x10
#define    CUSTOM_mType         0x1F
#define    mType_MASK           0x7F

/* Define the return codes */
#define SUCCESS      0x00
#define LISTISFULL   0x01
#define NOSDEPDEV    0x02
#define NOTRANS      0x03

/* Define the utility macro */
#define BULK_IN      0x00
#define BULK_OUT     0x01

/* Define internal macro */
#define AYABASETIME  1*1000

/* Define the signal macro */
#define SIGLISTCHANGE    SIGUSR1
#define SIGDEVINTERRUPT  SIGUSR2

/* Define the base description macro of module */
#define USB_SDEP_CNAME  "sdepPHY"
#define USB_SDEP_UNAME  "sdepUSBPHY"
#define USB_SDEP_DNAME  "sdepUSB"

/* Define bit manipulation macro */
#define SETBIT(data, bit) (data |= (1 << bit))
#define CLRBIT(data, bit) (data &= ~(1 << bit))
#define GETBIT(data, bit) ((data & (1 << bit)) >> bit)

/* Define status flag for SDEP device */
#define S_HANDSHAKE     0x00
#define S_RUNNING       0x01
#define S_DISCONNECT    0x02
#define S_NODEV         0x03

/* A printk redefinition for the DEBUG mode of the module */
#ifdef DEBUG
#define dprintk(...)    printk(__VA_ARGS__);
#else
#define dprintk(...)
#endif

/* Define of the IOCTL message */
#define IOCTL_MAGIC      'u'
// Use this iotctl command for get a quantity of attached device
#define IOCTL_HOWMANYDEVICE  _IO(IOCTL_MAGIC, 0x50) // With no parameters
// Use this iotctl command for get a list of attached device
#define IOCTL_GETLIST        _IOW(IOCTL_MAGIC, 0x51, char*) // From kernel module to user process
// Use this iotctl command for select the device where read and write the message
#define IOCTL_SELECT         _IOR(IOCTL_MAGIC, 0x52, int) // From user process to kernel module
// Use this iotctl command for get the interrupted table (16 byte)
#define IOCTL_POPNEXTINT     _IOW(IOCTL_MAGIC, 0x53, char*) // From user process to kernel module
// Use this iotctl command for forced physical disconnect of a probe
#define IOCTL_DISCONNECT     _IOR(IOCTL_MAGIC, 0x54, int) // From user process to kernel module

typedef struct {
    __u8                    dAddress;               // the device assigned address
    struct usb_device       *udev;                  // the usb device for this device
    struct usb_interface    *interface;             // the interface for this device
    __u8                    bulk_in_endpointAddr;   // the address of the bulk in endpoint
    __u8                    bulk_out_endpointAddr;  // the address of the bulk out endpoint
    struct {
        __u8                interrupted:1;          // if true the device is in intterrupted status
        __u8                syncTrans:1;            // if true a sync transaction om probe is in execution
        __u8                asyncTrans:1;           // if true a sync transaction om probe is in execution
        __u8                reserved:5;             // not used bits
    } flags;
    spinlock_t              sLock;                  // spinlock struct for syncronization between checkAlive and syncTrans
    unsigned long           transSTime;             // transaction start time
    struct urb              *transfer_urb;          // transfer urb user for async transaction
    __u8                    *transfer_buffer;       // send and receive buffer for async transaction
    __u8                    countedTimeout;         // number of consecutive timeout response
} sdepUSBPHY_t;

typedef struct {
    uint8_t pAddress;
    uint32_t pSDEPVer;
    uint32_t pSerial;
    int8_t pStatus;
    sdepUSBPHY_t *sdepUSB;
} sdepAddressTranslation_f;

typedef struct {
    uint8_t pAddress;
    uint32_t pSDEPVer;
    uint32_t pSerial;
} sdepAddressList_f;

void    initializeSAT(sdepAddressTranslation_f SAT[]);
int8_t  getFreeAddress(sdepAddressTranslation_f SAT[], uint8_t *pAddress);
int8_t  addressFromInterface(sdepAddressTranslation_f SAT[], struct usb_interface *interface, uint8_t *pAddress);

ssize_t usbphy_bulkrw(sdepUSBPHY_t *sdepUSBPHY, unsigned char io_dir, char *buffer, size_t len);
ssize_t usbphy_bulkrw_async(sdepUSBPHY_t *sdepUSBPHY, unsigned char io_dir, char *buffer, size_t len, usb_complete_t complete_fn);

int8_t  testGetSerial(sdepUSBPHY_t *sdepUSBPHY, uint32_t *pSDEPVer, uint32_t *pSerial);
int8_t  setAddress(sdepUSBPHY_t *sdepUSBPHY, uint8_t pAddress);
int8_t  sendDisconnect(sdepUSBPHY_t *sdepUSBPHY, uint8_t pAddress);

uint8_t popInt(uint8_t intQueue[]);
uint8_t pushInt(uint8_t intQueue[], uint8_t pAddress);
int8_t  send_signal(int pid, int sig_num);
#endif
