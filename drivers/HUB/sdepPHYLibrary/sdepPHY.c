/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @subproject: SDEP USB Physical Abstraction Layer v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepPHY.c
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include <sdepPHY.h>

// Pointer to device file
static int filepDrv;

/**
 * @brief	Open an istance of the driver
 * @param	None
 * @retval	SUCCESS if success else -ENODEV if error
 */
int32_t openDriver()
{
	// Open the device driver
	filepDrv=open(DEV_NAME, O_RDWR);
	if (filepDrv<1){
		return filepDrv;
	}
	
	return SUCCESS;
}

/**
 * @brief	Close the current istance of the driver
 * @param	None
 * @retval	SUCCESS if success else -ENODEV if error
 */
int32_t closeDriver()
{
	if (filepDrv>0){
		close(filepDrv);
		return SUCCESS;
	}
	
	return -ENODEV;
}

/**
 * @brief	Register an handler for the driver signal callback
 * @param	sig_num - number of associated signal
 * @param	sig_handler - function callback for this sig_num
 * @retval	SUCCESS if success else -EFAULT if error
 */
int32_t registerSignalHandler(int sig_num, void (*sig_handler)(int sig_num))
{	
	struct sigaction u_action;

	if (sig_handler == NULL)
		return -EFAULT;
			
	memset(&u_action, 0, sizeof(u_action));       // clean variable
	u_action.sa_handler = sig_handler;           // specify signal handler
	u_action.sa_flags = SA_NODEFER;                 // do not prevent the signal different from sig_num
	
	if (sigaction (sig_num, &u_action, NULL) < 0) { // attach action with sig_num
		return -EFAULT;
	} else {
		return SUCCESS;
	}
}

/**
 * @brief	Pop from the interrupted queue the head pAddress
 * @param	pAddress- poped pAddress
 * @retval	SUCCESS if success else -ENODEV if the queue is empty,
 *          -ENOMEM if you passed an incorrect address
 */
int32_t popNextInterrupted(uint8_t *pAddress)
{
	int32_t retVal;
	uint8_t popAddress;

	retVal = (int)ioctl(filepDrv, IOCTL_POPNEXTINT, &popAddress);

	*pAddress = popAddress;
	
	return retVal;
}

/**
 * @brief	Get and allocate an updated list of current attached device to the driver with address and serial
 * @param	attachedList - pointer a list of attached device
 * @param	attachedNum - number of elements in the list
 * @retval	SUCCESS if success else -EFAULT if error
 */

int32_t getAttachedList(sdepAddressList_f **attachedList, uint32_t *attachedNum)
{
	uint32_t countDev;
	uint32_t listNum;
	sdepAddressList_f* sdepAddressList;
	
	// Recover the number of the connected device
	countDev = (int)ioctl(filepDrv, IOCTL_HOWMANYDEVICE, 0);
	
	if (*attachedList != NULL) {
		free(*attachedList);
	}
	
	sdepAddressList=(sdepAddressList_f*)malloc(countDev*(sizeof(sdepAddressList_f)));
	
	// Recover the list of the connected device and print it
	listNum=(int)ioctl(filepDrv, IOCTL_GETLIST, sdepAddressList);

	if (countDev == listNum){
		*attachedNum=countDev;
		*attachedList=sdepAddressList;
		return SUCCESS;
	} else {
		*attachedNum=0;
		*attachedList=(void*)0;
		return -EFAULT;
	}
}

/**
 * @brief	Deallocate a previous allocated list of attached device
 * @param	attachedList - pointer a list of attached device
 * @retval	SUCCESS if success else -EFAULT if error
 */
int32_t freeAttachedList(sdepAddressList_f *attachedList)
{
	if (attachedList != NULL) {
		free(attachedList);
		return SUCCESS;
	}
	return -EFAULT;
}
	
/**
 * @brief	Force the disconnect of the selected device through its address
 * @param	pAddress - address of the selected device
 * @retval	number of attached PROBE device
 */
int32_t forceDisconnect(uint8_t pAddress){
	return (int)ioctl(filepDrv, IOCTL_DISCONNECT, pAddress);
}

// Physical callbacks

/**
 * @brief	Request to driver the number of attached device
 * @param	filed - pointer to a file descriptor of device
 * @retval	number of attached PROBE device
 */
int32_t selectAddress(uint8_t pAddress){
	return (int)ioctl(filepDrv, IOCTL_SELECT, pAddress);
}

/**
 * @brief	Request to driver the number of attached device
 * @param	filed - pointer to a file descriptor of device
 * @retval	number of attached PROBE device
 */
int32_t writeChars(const uint8_t *data, uint16_t length){
	return write(filepDrv, data, length);
}

/**
 * @brief	Request to driver the number of attached device
 * @param	filed - pointer to a file descriptor of device
 * @retval	number of attached PROBE device
 */
int32_t readChars(uint8_t *data, uint16_t length){
	return read(filepDrv, data, length);
}
