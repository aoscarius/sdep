/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @subproject: SDEP USB Physical Driver v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepUSBPHY_lib.c
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include <linux/init.h>           // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>         // Core header for loading LKMs into the kernel
#include <linux/device.h>         // Header to support the kernel Driver Model
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
#include <linux/fs.h>             // Header for the Linux file system support
#include <linux/types.h>          // Typedef for uintX_t
#include <asm/uaccess.h>          // Required for the copy to user function
#include <linux/slab.h>           // Required for the kmalloc

#include <linux/sched.h>          // Required for recovery task from pid
#include <linux/signal.h>         // Required for signal functionality
#include <asm/siginfo.h>          // Required for signal struct info

#include <linux/timer.h>          // Support for timer functionality

#include "sdepPHYUSB.h"           // Support utility

static uint8_t intHead=0;
static uint8_t intTail=0;

extern int usbTimeout;

/**
 * @brief    Reset and initialize a SDEP Address Translation table
 * @param    SAT - the SAT to inizialize
 * @retval   none
 */
void initializeSAT(sdepAddressTranslation_f SAT[]){
    uint8_t i = 0;
    for (i=0; i<MAX_ENTRIES; i++){
        SAT[i].pAddress = BADDRESS;
        SAT[i].pSerial = 0;
        SAT[i].pStatus = S_NODEV;
        SAT[i].sdepUSB = NULL;
    }
}

/**
 * @brief    Recover the first free address from the passed SAT
 * @param    SAT - the SAT to interrogate
 * @param    pAddress - the returned found address if success
 * @retval   SUCCESS if the address is found else -LISTISFULL
 */
int8_t getFreeAddress(sdepAddressTranslation_f SAT[], uint8_t *pAddress)
{
    uint8_t i = 0;
    
    while ((SAT[i].pAddress != BADDRESS) && (i < MAX_ENTRIES)){
        i++;
    }

    if (i<MAX_ENTRIES) {
        *pAddress = i;
        return SUCCESS;
    } else {
        *pAddress = 0;
        return -LISTISFULL;
    }
}

/**
 * @brief    Recover the address of the specified interface from the passed SAT
 * @param    SAT - the SAT to interrogate
 * @param    interface - usb interface to check
 * @param    pAddress - the returned found address if success
 * @retval   SUCCESS if the interface is found else -ENODEV
 */
int8_t addressFromInterface(sdepAddressTranslation_f SAT[], struct usb_interface *interface, uint8_t *pAddress)
{
    uint8_t i = 0;
    

    while (i < MAX_ENTRIES){
        if ((SAT[i].pAddress!=BADDRESS) && (SAT[i].sdepUSB != NULL) && (SAT[i].sdepUSB->interface == interface)) break;
        i++;
    }
    
    if (i<MAX_ENTRIES) {
        *pAddress = i;
        return SUCCESS;
    } else {
        *pAddress = 0;
        return -ENODEV;
    }
}

/**
 * @brief    Request to usb core a BULK transfer in IN or OUT through an automatic URB
 * @param    sdepUSBPHY - pointer to implied sdepPHY device
 * @param    io_dir - direction of transfer
 * @param    buffer - pointer to the buffer implied in transfer
 * @param    len - len of the pointed buffer
 * @retval   number of transferred bytes if success else a negative number if error
 */
ssize_t usbphy_bulkrw(sdepUSBPHY_t *sdepUSBPHY, unsigned char io_dir, char *buffer, size_t len)
{
    int retVal = 0;
    int count = len;

    if (!sdepUSBPHY)
        return -ENODEV;

    if (!((io_dir == BULK_IN) || (io_dir == BULK_OUT)))
        return -EFAULT;


    if (io_dir == BULK_IN){
        /* do a blocking bulk read to get data from the device */
        retVal = usb_bulk_msg(sdepUSBPHY->udev, usb_rcvbulkpipe(sdepUSBPHY->udev, sdepUSBPHY->bulk_in_endpointAddr), buffer, len, &count, usbTimeout);
    
    } else if (io_dir == BULK_OUT) {
        /* do a blocking bulk write to put data to the device */
        retVal = usb_bulk_msg(sdepUSBPHY->udev, usb_sndbulkpipe(sdepUSBPHY->udev, sdepUSBPHY->bulk_out_endpointAddr), buffer, len, &count, usbTimeout);
    }
    
    if (!retVal) {
        retVal = count;
    } 

    return retVal;
}

/**
 * @brief    Request to usb core a BULK transfer in IN or OUT through an async URB
 * @param    sdepUSBPHY - pointer to implied sdepPHY device
 * @param    io_dir - direction of transfer
 * @param    buffer - pointer to the buffer implied in transfer
 * @param    len - len of the pointed buffer
 * @param    complete_fn - the callback function to run at transfer complete
 * @retval   number of transferred bytes if success else a negative number if error
 */
ssize_t usbphy_bulkrw_async(sdepUSBPHY_t *sdepUSBPHY, unsigned char io_dir, char *buffer, size_t len, usb_complete_t complete_fn)
{
    int retVal = 0;
    unsigned int pipe;

    if (!sdepUSBPHY)
        return -ENODEV;

    if (!((io_dir == BULK_IN) || (io_dir == BULK_OUT)))
        return -EFAULT;

    sdepUSBPHY->transfer_urb = usb_alloc_urb(0, GFP_ATOMIC);
    if (!sdepUSBPHY->transfer_urb)
        return -ENOMEM;
	
    if (io_dir == BULK_IN){
        // recover the IN pipe endopoint address
        pipe = usb_rcvbulkpipe(sdepUSBPHY->udev, sdepUSBPHY->bulk_in_endpointAddr);

    } else if (io_dir == BULK_OUT) {
        // recover the OUT pipe endopoint address
        pipe = usb_sndbulkpipe(sdepUSBPHY->udev, sdepUSBPHY->bulk_out_endpointAddr);

    }
    
    // setup the urb
    usb_fill_bulk_urb(sdepUSBPHY->transfer_urb, sdepUSBPHY->udev, pipe, buffer, len, complete_fn, sdepUSBPHY);

    // init the buffer transmission
    retVal = usb_submit_urb(sdepUSBPHY->transfer_urb, GFP_ATOMIC);
    
    return retVal;
}

/**
 * @brief    Interrogate a device in the handshake phase to check if it's a SDEP device and recover its serial
 * @param    sdepUSBPHY - pointer to implied sdepPHY device
 * @param    pSerial - the returned pSerial if success
 * @retval   SUCCESS if success esle -NOSDEPDEV if it's not a SDEP device
 */
int8_t testGetSerial(sdepUSBPHY_t *sdepUSBPHY, uint32_t *pSDEPVer, uint32_t *pSerial)
{
    uint8_t serialMessage[] = "\x00\xFF\x00\x00";
    uint8_t rcvBuffer[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    uint8_t *rcvBufferPtr;
    uint32_t sdepMagic;
    
    uint32_t ioBytes;
    
    serialMessage[0]=WHO_mType;
    serialMessage[1]=BADDRESS;

    ioBytes = usbphy_bulkrw(sdepUSBPHY, BULK_OUT, serialMessage, 4);
    ioBytes = usbphy_bulkrw(sdepUSBPHY, BULK_IN, rcvBuffer, 4);
    ioBytes = usbphy_bulkrw(sdepUSBPHY, BULK_IN, &(rcvBuffer[4]), 12);

    rcvBufferPtr = &(rcvBuffer[4]);

    sdepMagic = *((uint32_t*)(rcvBufferPtr));
    *pSDEPVer = *((uint32_t*)(rcvBufferPtr+4));
    *pSerial = *((uint32_t*)(rcvBufferPtr+8));
    
    if (sdepMagic == 0x53444550) {
        dprintk(KERN_INFO USB_SDEP_DNAME ": SDEP PROBE device recognized with serial %08X\n", *pSerial);
        dprintk(KERN_INFO USB_SDEP_DNAME ": SDEP PROBE version %08X\n", *pSDEPVer);
        return SUCCESS;
    } else {
        *pSerial = 0;
        dprintk(KERN_INFO USB_SDEP_DNAME ": no SDEP PROBE device recognized\n");
        return -NOSDEPDEV;
    }
}

/**
 * @brief    Send a YOUARE message to a specified device
 * @param    sdepUSBPHY - pointer to implied sdepPHY device
 * @param    pAddress - address to set for the specified device
 * @retval   SUCCESS if success else -EFAULT if error
 */
int8_t setAddress(sdepUSBPHY_t *sdepUSBPHY, uint8_t pAddress)
{
    uint8_t addressMessage[] = "\x00\xFF\x01\x00\x00";
    uint8_t rcvBuffer[4] = {0, 0, 0, 0};
    uint32_t ioBytes;
    
    addressMessage[0]=YOUARE_mType;
    addressMessage[1]=BADDRESS;
    addressMessage[4]=pAddress;
   
    ioBytes = usbphy_bulkrw(sdepUSBPHY, BULK_OUT, addressMessage, 5);
    ioBytes = usbphy_bulkrw(sdepUSBPHY, BULK_IN, rcvBuffer, 4);
    
    if (((rcvBuffer[0] & mType_MASK) == ACK_mType) && (rcvBuffer[1] == pAddress)) {
        dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE at address %x accept its address\n", pAddress);
        return SUCCESS;
    } else {
        dprintk(KERN_INFO USB_SDEP_DNAME ": PROBE at address %x refuse its address\n", pAddress);
        return -EFAULT;
    }
}

/**
 * @brief    Send a disconnect request to a specified device
 * @param    sdepUSBPHY - pointer to implied sdepPHY device
 * @param    pAddress - its address for test
 * @retval   number of attached PROBE device
 */
int8_t sendDisconnect(sdepUSBPHY_t *sdepUSBPHY, uint8_t pAddress)
{
    uint8_t disconnectMessage[] = "\x00\xFF\x00\x00";
    uint8_t rcvBuffer[4] = {0, 0, 0, 0};
    
    uint32_t ioBytes;
    
    disconnectMessage[0]=BYEBYE_mType;
    disconnectMessage[1]=pAddress;
    
    ioBytes = usbphy_bulkrw(sdepUSBPHY, BULK_OUT, disconnectMessage, 4);
    ioBytes = usbphy_bulkrw(sdepUSBPHY, BULK_IN, rcvBuffer, 4);
    if (((rcvBuffer[0] & mType_MASK) == ACK_mType) && (rcvBuffer[1] == pAddress)) {
        return SUCCESS;
    } else {
        return -EFAULT;
    }
}

/**
 * @brief    Pop an address from the head of interrupt queue if it's not empty
 * @param    intQueue - the interrupt queue
 * @retval   Poped address from head of queue if success else BADDRESS if queue is empty
 */
uint8_t popInt(uint8_t intQueue[])
{
    uint8_t retVal;
    
    if (intQueue[intHead] != BADDRESS){
        retVal = intQueue[intHead];
        intQueue[intHead] = BADDRESS;
        intHead=(intHead + 1) % MAX_ENTRIES;
        dprintk(KERN_ALERT USB_SDEP_DNAME ": PROBE with address %x poped from intQueue\n", retVal);
    } else {
        retVal = BADDRESS;
    }
    
    return retVal;
}

/**
 * @brief    Push an address in the tail of interrupt queue if it's not full
 * @param    intQueue - the interrupt queue
 * @param    pAddress - address to push in tail
 * @retval   Pushed address into the tail of queue if success else BADDRESS if queue is empty
 */
uint8_t pushInt(uint8_t intQueue[],uint8_t pAddress)
{
    uint8_t retVal;

    if (intQueue[intTail] == BADDRESS){
        retVal = pAddress;
        intQueue[intTail] = pAddress;
        intTail=(intTail + 1) % MAX_ENTRIES;
        dprintk(KERN_ALERT USB_SDEP_DNAME ": PROBE with address %x pushed into intQueue\n", pAddress);
    } else {
        retVal = BADDRESS;
    }
    
    return retVal;
}

/**
 * @brief    Send a speficied signal to a specified PID process
 * @param    pid - PID of the process to send signal
 * @param    sig_num - number of the signal
 * @retval   SUCCESS if success else -EFAULT if error
 */
int8_t send_signal(int pid, int sig_num)
{
    int retVal;
    struct siginfo info;
    struct task_struct *task;

    if (pid != 0) {

        dprintk(KERN_ALERT USB_SDEP_DNAME ": sending triggering signal to %d\n", pid);

        memset(&info, 0, sizeof(struct siginfo));

        info.si_signo = sig_num;
        info.si_code = SI_USER;

        task = pid_task(find_vpid(pid), PIDTYPE_PID);

        retVal = send_sig_info(sig_num, &info, task);

        if (retVal<0) {
            printk(KERN_ALERT USB_SDEP_DNAME ": error sending signal\n");
            return -EFAULT;
        } else {
            return SUCCESS;
        }
    }
    
    return -EFAULT;
}
