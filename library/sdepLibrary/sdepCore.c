/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepCore.c
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#include <string.h>

#include "sdepCore.h"
#include "sdepConfig.h"
#include "sdepProtocol.h"
#include "sdepMessage.h"

#if setMASTER == 0
// -----------------------------
// ------ Slave Functions ------
// -----------------------------

// ----------------------------------------
// ----------- sdepCore Private -----------
// ----------------------------------------

typedef enum {
    sHandshake,
    sRunning,
    sDisconnect
} sdepCoreState_t;

sdepCore_t sdepCore;
sdepCoreState_t stateMachine = sHandshake;

// ----------- Support Function -----------
uint32_t sendMagicSerial();
uint8_t getAddressFrom(sdepMessage_t *message);
sId_t getSensorIdFrom(sdepMessage_t *message);
int32_t sendAliveAck();

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

/**
 * @brief   Set the interrupt bit for int request
 * @param   None
 * @retval  None
 */
void  setInterrupt(){
    sdepCore.flags.pIntEnabled = 1;
}

/**
 * @brief   Clear the interrupt bit
 * @param   None
 * @retval  None
 */
void clearInterrupt(){
    sdepCore.flags.pIntEnabled = 0;
}

/**
 * @brief   Return the value of interrupt bit
 * @param   None
 * @retval  The value of interrupt bit
 */
int8_t isInterrupted(){
    return sdepCore.flags.pIntEnabled;
}

/**
 * @brief   Initialize the sdepCore machine and attach the callback function.
 * @param   pSerial - the serial to assign at this PROBE
 * @param   phyDriver - a phyDriver initialized callbacks
 * @param   sdepCallbacks - a sdepCore initialized callbacks
 * @retval  sdepOK if all is correct else -sdepERROR
 */
int8_t sdepInitCore(pSerial_t pSerial, phyDriver_t phyDriver, sdepCallbacks_t sdepCallbacks){
    // Check the passed parameters
    if (!pSerial) return -sdepERROR;

    if ((!phyDriver.writeBytes) || (!phyDriver.readBytes))
        return -sdepERROR;

    if ((!sdepCallbacks.capabilityRequest) || (!sdepCallbacks.configRequest) ||    (!sdepCallbacks.samplesRequest))
        return -sdepERROR;

    // Set the SDEP Magic to identify the protocol
    sdepCore.sMagic = SDEPM;
    sdepCore.sMagicVer = SDEPVER;

    // Initialize the address and serial of PROBE
    sdepCore.mAddress = sdepBADDRESS;
    sdepCore.pSerial = pSerial;
    sdepCore.flags.pIntEnabled = 0;
    
    // Attach the callbacks function of the driver
    sdepCore.phyDriver.writeBytes = phyDriver.writeBytes;
    sdepCore.phyDriver.readBytes = phyDriver.readBytes;

    // Attach the callbacks function of state machine
    sdepCore.sdepCallbacks.aliveCheck = sdepCallbacks.aliveCheck;
    sdepCore.sdepCallbacks.capabilityRequest = sdepCallbacks.capabilityRequest;
    sdepCore.sdepCallbacks.configRequest = sdepCallbacks.configRequest;
    sdepCore.sdepCallbacks.samplesRequest = sdepCallbacks.samplesRequest;
    sdepCore.sdepCallbacks.samplesRequestInt = sdepCallbacks.samplesRequestInt;
    sdepCore.sdepCallbacks.customRequest = sdepCallbacks.customRequest;
    sdepCore.sdepCallbacks.probeDisconnect = sdepCallbacks.probeDisconnect;
    
    // Initialize the state machine
    stateMachine = sHandshake;

    return sdepOK;
}

/**
 * @brief   Execute a single step of a sdepCore state machine
 * @param   None
 * @retval  sdepOK or if a bad message is intercept its return -sdepBADMESSAGE
 */
int8_t sdepCoreMachineStep(){
    int8_t retVal = sdepOK;
    sdepMessage_t *sdepMessage;
    int32_t recvBytes;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = NULL_mType;

    sdepMessage = newMessage(mType, 0, 0, (void*)0);

    recvBytes = recvMessage(sdepMessage);
    // Check if this message is a possible sdepMessage
    if ((recvBytes >= headerSIZE) && (stateMachine!=sDisconnect)) {
        // Check if sdepMessage is formatted correctly
        if (recvBytes == (headerSIZE+sdepMessage->pLength)){
            // Check if this message is for me
            if (sdepMessage->mAddress == sdepCore.mAddress) {
                // Enter in the sdepCore state machine
                switch(stateMachine){
                    /**
                     * In the sHandshake state only WHO and YOUARE message are accepted.
                     * Only when a new address is assigned to the PROBE, the machine step up.
                     */
                    case sHandshake:
                        /**
                         * If a IAM message is not received, it's possibile in this phase resend
                         * a WHO message. Only a correct YOUARE message change the state of machine.
                         */
                        switch(sdepMessage->mType.msgType){
                            // Verify the WHO message and call sendSerial();
                            case WHO_mType:
                                sendMagicSerial();
                                break;
                            // Verify the YOUARE message and call sendAck(), stateMachine = sRunning;
                            case YOUARE_mType:
                                sdepCore.mAddress = getAddressFrom(sdepMessage);
                                sendAck();
                                stateMachine = sRunning;
                                break;
                            // Any other type of message or incorrect message are simply ignored.
                            default:
                                break;
                        }
                        break;
                    /**
                     * In the sRunning state, any others type of well formatted sdepMessage are accepted.
                     * The particular BYEBYE message, force the system to a software disconnect and change the state
                     * of machine in a state where all message are ignored. Only a re-initialization of the core can
                     * restart the machine.
                     */
                    case sRunning:
                        /**
                         * AREYOUALIVE message is self-managed. Any other message put off the task of running the
                         * event to the relevant callback calls.
                         */
                        switch(sdepMessage->mType.msgType){
                            // Verify AREYOUALIVE message and sendAck(), after call the aliveCheck() callback if the PROBE as defined it
                            case AREYOUALIVE_mType:
                                sendAliveAck();
                                if (sdepCore.sdepCallbacks.aliveCheck != (void*)0){
                                    retVal = sdepCore.sdepCallbacks.aliveCheck();
                                }
                                break;
                            // Verify WHAT message and call the capabilityRequest() callback if the PROBE as defined it
                            case WHAT_mType:
                                retVal = sdepCore.sdepCallbacks.capabilityRequest();
                                break;
                            // Verify CONF message and call the configRequest() callback if the PROBE as defined it
                            case CONF_mType:
                                retVal = sdepCore.sdepCallbacks.configRequest((sConfig_t*)sdepMessage->mPayload, (sdepMessage->pLength/sizeof(sConfig_t)));
                                break;
                            // Verify DREQ message and call the samplesRequest() callback if the PROBE as defined it
                            case DREQ_mType:
                                retVal = sdepCore.sdepCallbacks.samplesRequest(getSensorIdFrom(sdepMessage));
                                break;
                            // Verify DREQIT message and call the samplesRequestInt() callback if the PROBE as defined it
                            case DREQIT_mType:
                                if (sdepCore.sdepCallbacks.samplesRequestInt != (void*)0){
                                    retVal = sdepCore.sdepCallbacks.samplesRequestInt();
                                }
                                break;
                            // Verify CUSTOM message and call the customRequest() callback if the PROBE as defined it
                            case CUSTOM_mType:
                                if (sdepCore.sdepCallbacks.customRequest != (void*)0){
                                    retVal = sdepCore.sdepCallbacks.customRequest(sdepMessage->mPayload, sdepMessage->pLength);
                                }
                                break;
                            // Verify BYEBYE message, call the probeDisconnect() callback  if the PROBE as defined it and reset the PROBE address, stateMachine = sDisconnect;
                            case BYEBYE_mType:
                                if (sdepCore.sdepCallbacks.probeDisconnect != (void*)0){
                                    retVal = sdepCore.sdepCallbacks.probeDisconnect();
                                    sdepCore.mAddress = sdepBADDRESS;
                                    stateMachine = sDisconnect;
                                }
                                break;
                            // Any other type of message or incorrect message are simply ignored.
                            default:
                                break;
                        }

                        break;
                    /**
                     * In the sDisconnect state, any message are ignored.
                     */
                    case sDisconnect:
                        //while(1) {
                        //}
                        break;
                    //
                    default:
                        break;
                }
            }

        } else {
            // If the message is malformed a null-payloaded error is send
            sendError((void*)0, 0);
            retVal = -sdepBADMESSAGE;
        }
    }

    freeMessage(sdepMessage);
    
    return retVal;
}

/**
 * @brief   Send an IAM message to the Master
 * @param   None
 * @retval  True if sdepCoreMachine running, else false
 */
int8_t sdepMachineIsRunning(){
    return (stateMachine == sRunning);
}

/**
 * @brief   Send an IAM message to the Master
 * @param   None
 * @retval  Number of incomplete sending byte
 */
uint32_t sendMagicSerial(){
    sdepMessage_t *message;
    mPayload_t mPayload[sizeof(sdepCore.sMagic) + sizeof(sdepCore.sMagicVer) + sizeof(pSerial_t)];
    uint16_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = IAM_mType;

    pLength = sizeof(sdepCore.sMagic) + sizeof(sdepCore.sMagicVer) + sizeof(pSerial_t);

    memcpy(&(mPayload[0]), &sdepCore.sMagic, sizeof(sdepCore.sMagic));
    memcpy(&(mPayload[sizeof(sdepCore.sMagic)]), &sdepCore.sMagicVer, sizeof(sdepCore.sMagicVer));
    memcpy(&(mPayload[sizeof(sdepCore.sMagic)+sizeof(sdepCore.sMagic)]), &sdepCore.pSerial, sizeof(sdepCore.pSerial));

    message = newMessage(mType, sdepCore.mAddress, pLength, mPayload);
    txByte=sendMessage(message);

    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

/**
 * @brief   Recover the mAddress from a payload of sdepMessage
 * @param   sdepMessage to analize
 * @retval  mAddress
 */
mAddress_t getAddressFrom(sdepMessage_t *message){
    return *((uint8_t*)message->mPayload);
}

/**
 * @brief   Recover the sensorId from a payload of sdepMessage
 * @param   sdepMessage to analize
 * @retval  sensorId
 */
sId_t getSensorIdFrom(sdepMessage_t *message){
    return *((sId_t*)message->mPayload);
}

/**
 * @brief   Send an ALIVEACK to the Master
 * @param   None
 * @retval  Zero on success, a negative number if error or number of incomplete sending byte
 */
int32_t sendAliveAck(){
    sdepMessage_t *message;
    pLength_t pLength;
    uint32_t txByte;
    mType_t mType;

    mType.intEnabled = sdepCore.flags.pIntEnabled;
    mType.msgType = ALIVEACK_mType;

    pLength = 0;

    message = newMessage(mType, sdepCore.mAddress, pLength, (void*)0);
    txByte=sendMessage(message);

    freeMessage(message);

    return ((headerSIZE + pLength) - txByte);
}

#else
// -----------------------------
// ------ Master Functions -----
// -----------------------------

// ----------------------------------------
// ----------- sdepCore Private -----------
// ----------------------------------------

sdepCore_t sdepCore;

// ----------- Support Function -----------
sId_t getSensorIdFrom(sdepMessage_t *message);

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

/**
 * @brief   Initialize the sdepCore machine and attach the callback function.
 * @param   phyDriver - a phyDriver initialized callbacks
 * @param   sdepCallbacks - a sdepCore initialized callbacks
 * @retval  sdepOK if all is correct else -sdepERROR
 */
int8_t sdepInitCore(phyDriver_t phyDriver, sdepCallbacks_t sdepCallbacks){
    // Check the passed parameters
    if ((!phyDriver.selectAddress) || (!phyDriver.writeBytes) || (!phyDriver.readBytes))
        return -sdepERROR;

    if ((!sdepCallbacks.ackResponse)        || (!sdepCallbacks.errorResponse)       ||
        (!sdepCallbacks.capabilityResponse) || (!sdepCallbacks.dataArriveResponse))
        return -sdepERROR;

    // Attach the callbacks function of the driver
    sdepCore.phyDriver.selectAddress = phyDriver.selectAddress;
    sdepCore.phyDriver.writeBytes = phyDriver.writeBytes;
    sdepCore.phyDriver.readBytes = phyDriver.readBytes;

    // Attach the callbacks function of state machine
    sdepCore.sdepCallbacks.ackResponse = sdepCallbacks.ackResponse;
    sdepCore.sdepCallbacks.errorResponse = sdepCallbacks.errorResponse;
    sdepCore.sdepCallbacks.errorResponseInt = sdepCallbacks.errorResponseInt;
    sdepCore.sdepCallbacks.capabilityResponse = sdepCallbacks.capabilityResponse;
    sdepCore.sdepCallbacks.dataArriveResponse = sdepCallbacks.dataArriveResponse;
    sdepCore.sdepCallbacks.dataArriveResponseInt = sdepCallbacks.dataArriveResponseInt;
    sdepCore.sdepCallbacks.customResponse = sdepCallbacks.customResponse;
    
    return sdepOK;
}

/**
 * @brief   Execute a single step of a sdepCore state machine
 * @param   pAddress - address of the PROBE to read from
 * @retval  sdepOK or if a bad message is intercept its return -sdepBADMESSAGE
 */
int8_t sdepCoreResponseDecoder(mAddress_t pAddress){
    int8_t retVal = sdepOK;
    sdepMessage_t *sdepMessage;
    int32_t recvBytes;
    mType_t mType;

    mType.intEnabled = 0;
    mType.msgType = NULL_mType;
    
    if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
        return -sdepNODEVADDR;

    sdepMessage = newMessage(mType, 0, 0, (void*)0);

    recvBytes = recvMessage(sdepMessage);
    // Check if this message is a possible sdepMessage
    if (recvBytes >= headerSIZE) {
        // Check if sdepMessage is formatted correctly
        if (recvBytes == (headerSIZE+sdepMessage->pLength)){
                // Enter in the sdepCore response decoder
                switch(sdepMessage->mType.msgType){
                    // Verify ACK message call the ackResponse() callback
                    case ACK_mType:
                        retVal = sdepCore.sdepCallbacks.ackResponse(sdepMessage->mAddress);
                        break;
                    // Verify ERROR message and call the errorResponse() callback
                    case ERROR_mType:
                        retVal = sdepCore.sdepCallbacks.errorResponse(sdepMessage->mAddress, (sError_t*)sdepMessage->mPayload, (sdepMessage->pLength/sizeof(sError_t)));
                        break;
                    // Verify ERRORIT message and call the errorResponseInt() callback if the MASTER as defined it
                    case ERRORIT_mType:
                        if (sdepCore.sdepCallbacks.errorResponseInt != (void*)0){
                            retVal = sdepCore.sdepCallbacks.errorResponseInt(sdepMessage->mAddress);
                        }
                        break;
                    // Verify MYCAP message and call the capabilityResponse() callback
                    case MYCAP_mType:
                        retVal = sdepCore.sdepCallbacks.capabilityResponse(sdepMessage->mAddress, (sCapability_t*)sdepMessage->mPayload, (sdepMessage->pLength/sizeof(sCapability_t)));
                        break;
                    // Verify DEXG message and call the dataArriveResponse() callback
                    case DEXG_mType:
                        retVal = sdepCore.sdepCallbacks.dataArriveResponse(sdepMessage->mAddress, getSensorIdFrom(sdepMessage), (uint8_t*)(sdepMessage->mPayload+sizeof(sId_t)), (sdepMessage->pLength-sizeof(sId_t)));
                        break;
                    // Verify DEXGIT message and call the dataArriveResponseInt() callback if the MASTER as defined it
                    case DEXGIT_mType:
                        if (sdepCore.sdepCallbacks.dataArriveResponseInt != (void*)0){
                            retVal = sdepCore.sdepCallbacks.dataArriveResponseInt(sdepMessage->mAddress, getSensorIdFrom(sdepMessage), *((uint8_t*)(sdepMessage->mPayload+sizeof(sId_t))), (uint8_t*)(sdepMessage->mPayload+sizeof(sId_t)+sizeof(uint8_t)), (sdepMessage->pLength - sizeof(sId_t) - sizeof(uint8_t)));
                        }
                        break;
                    // Verify CUSTOM message and call the customRresponse() callback if the MASTER as defined it
                    case CUSTOM_mType:
                        if (sdepCore.sdepCallbacks.customResponse != (void*)0){
                            retVal = sdepCore.sdepCallbacks.customResponse(sdepMessage->mAddress, sdepMessage->mPayload, sdepMessage->pLength);
                        }
                        break;
                    // Any other type of message or incorrect message are simply ignored.
                    default:
                        break;
                }
                
                if (retVal == sdepOK) retVal = sdepMessage->mType.msgType;

        } else {
            retVal = -sdepBADMESSAGE;
        }
    }

    freeMessage(sdepMessage);

    return retVal;
}

/**
 * @brief   Recover the sensorId from a payload of sdepMessage
 * @param   sdepMessage to analize
 * @retval  sensorId
 */
sId_t getSensorIdFrom(sdepMessage_t *message){
    return *((sId_t*)message->mPayload);
}

#endif //setMASTER
