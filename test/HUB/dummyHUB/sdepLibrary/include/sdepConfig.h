/**
 * @project: SDEP Simple Data Exchange Protocol v0.9.0-rc
 *
 * @author:  Castello Oscar
 *           Iorio Raffaele
 *
 * @file:    sdepConfig.h
 *
 * @date:    18 July 2016
 *
 * @license: This work is licensed under the Creative Commons
 *           Attribution-NonCommercial-ShareAlike 4.0 International License.
 *           To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 */

#ifndef SDEP_CONFIG_H
#define SDEP_CONFIG_H

/**
 * Use this configuration for correct settings of the library.
 * Use setMASTER for select the Master or Slave version of the library.
 * If the Slave version is set, you can use uSDEP for device with small size RAM
 * by setting the useMICROSDEP to 1.
 * If you want, you can use or not the malloc operated version of the library
 * by setting the useMALLOC flag.
 */

// If is set the master function are enabled and the slave function are disabled
#define setMASTER      1

// If is set the master function are enabled and the slave function are disabled
#define setMICROSDEP   0

// If is set the malloc function is used for the sdepMessage
#define useMALLOC      1

#endif //SDEP_CONFIG_H
